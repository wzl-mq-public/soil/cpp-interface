var _figure_8h =
[
    [ "SOIL::Figure< T, x, y >", "class_s_o_i_l_1_1_figure.html", "class_s_o_i_l_1_1_figure" ],
    [ "datatype", "_figure_8h.html#a485afdb40b0c76b73cbbc0da177cf8e3", null ],
    [ "datatype< bool >", "_figure_8h.html#a034d9e6f234755509cc20b01b506f761", null ],
    [ "datatype< double >", "_figure_8h.html#a863aac9ac9eb4396135f86bd9bc29bad", null ],
    [ "datatype< int >", "_figure_8h.html#a1e904aedc828f75201ffc0452ff28c03", null ],
    [ "datatype< int64_t >", "_figure_8h.html#a980491413d86c50ced57a8345c8f63ed", null ],
    [ "datatype< SOIL::ENUM >", "_figure_8h.html#a3656e92123956c8afcfbe545eef81eb7", null ],
    [ "datatype< SOIL::TIME >", "_figure_8h.html#aa16db0f32c1f6a992261e1747be93097", null ],
    [ "datatype< std::string >", "_figure_8h.html#aa97c25e5a3e597eea65e13ac0ce85416", null ]
];