var indexSectionsWithContent =
{
  0: "_abcdefhijklmnopqrstuvw~",
  1: "bcefhmoprstv",
  2: "hmsu",
  3: "bcefhjlmoprstv",
  4: "abcdefhijmnoprstuvw~",
  5: "acdhkmnopqrstuvw",
  6: "bdeijmrst",
  7: "o",
  8: "_p",
  9: "st"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Friends",
  8: "Macros",
  9: "Pages"
};

