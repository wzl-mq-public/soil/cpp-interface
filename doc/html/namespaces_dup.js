var namespaces_dup =
[
    [ "HTTP", "namespace_h_t_t_p.html", "namespace_h_t_t_p" ],
    [ "MQTT", "namespace_m_q_t_t.html", "namespace_m_q_t_t" ],
    [ "mqtt", "namespacemqtt.html", null ],
    [ "SIGN", "namespace_s_i_g_n.html", "namespace_s_i_g_n" ],
    [ "SOIL", "namespace_s_o_i_l.html", "namespace_s_o_i_l" ],
    [ "UDP", "namespace_u_d_p.html", "namespace_u_d_p" ]
];