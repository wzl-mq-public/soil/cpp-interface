var class_s_o_i_l_1_1_function =
[
    [ "Function", "class_s_o_i_l_1_1_function.html#add2a16248e512d8efe813f2a27df73b1", null ],
    [ "~Function", "class_s_o_i_l_1_1_function.html#a8082d73f3dcb5567775dc2db5c291aec", null ],
    [ "add_argument", "class_s_o_i_l_1_1_function.html#a6e6b923ef2cd3c534e831719ac6bbdb0", null ],
    [ "add_argument", "class_s_o_i_l_1_1_function.html#a7705b93afa26a8a3d3012d88a9fea04d", null ],
    [ "add_return", "class_s_o_i_l_1_1_function.html#ac3753d06d5d191ed95e63df56f51f680", null ],
    [ "handle_get", "class_s_o_i_l_1_1_function.html#a636a905c115a1686caef9815c5b1aac9", null ],
    [ "handle_post", "class_s_o_i_l_1_1_function.html#a5d0ba47fb669127afcbea1a3d192fe88", null ],
    [ "invoke", "class_s_o_i_l_1_1_function.html#ac4d8391f2f588a467502f0bf9c8c7c7b", null ],
    [ "make_argument", "class_s_o_i_l_1_1_function.html#a542306cc71842ef1fe4b13597a5b7d1b", null ],
    [ "make_return", "class_s_o_i_l_1_1_function.html#a4cdaaf26db414f721aa0c79aaa38cd46", null ],
    [ "ptr", "class_s_o_i_l_1_1_function.html#a3fb541cbb68722263ff8d7ab9628a260", null ],
    [ "wjson", "class_s_o_i_l_1_1_function.html#add97197a51b3c44f9eb873a23329983e", null ]
];