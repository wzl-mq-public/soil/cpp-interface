var namespace_h_t_t_p =
[
    [ "Resource", "class_h_t_t_p_1_1_resource.html", "class_h_t_t_p_1_1_resource" ],
    [ "Server", "class_h_t_t_p_1_1_server.html", "class_h_t_t_p_1_1_server" ],
    [ "Json", "namespace_h_t_t_p.html#a317431a644c9db16a1657ecc77677a11", null ],
    [ "Methods", "namespace_h_t_t_p.html#a28a83748614f1c141821ce4b9da83d3f", null ],
    [ "Request", "namespace_h_t_t_p.html#ab1754212af3bb8edc420a97cffb176ea", null ],
    [ "Response", "namespace_h_t_t_p.html#af7f55b32590882bc9b905c78799fabdf", null ],
    [ "Status", "namespace_h_t_t_p.html#af603c787079c0b8cacc07b6be6ae5e95", null ]
];