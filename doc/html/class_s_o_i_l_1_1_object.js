var class_s_o_i_l_1_1_object =
[
    [ "Object", "class_s_o_i_l_1_1_object.html#ac821d8158a2c4214b15cb1d9be8954dd", null ],
    [ "~Object", "class_s_o_i_l_1_1_object.html#ab0d8fb2cb9e32c16ea631ed93e7a5d57", null ],
    [ "handle_delete", "class_s_o_i_l_1_1_object.html#a4e237fb0358f5adaea25f2ac95b8b5e7", null ],
    [ "handle_get", "class_s_o_i_l_1_1_object.html#a24b2c5323d9f5f0651d48d3d319eb225", null ],
    [ "handle_put", "class_s_o_i_l_1_1_object.html#ac6614134cf7d4c7caaaae13fac999978", null ],
    [ "insert", "class_s_o_i_l_1_1_object.html#a5dabf727c8256928d1f019520a34be1f", null ],
    [ "ptr", "class_s_o_i_l_1_1_object.html#a98e9761c2be5e2acdfe673f48259172e", null ],
    [ "read", "class_s_o_i_l_1_1_object.html#a0d2c97d0d9a71ec307211897d596bbde", null ],
    [ "remove", "class_s_o_i_l_1_1_object.html#a5480dbba0b410666dd030006e86b1108", null ],
    [ "wjson", "class_s_o_i_l_1_1_object.html#aebb0f522c4ced342fbd33cb377eeb4b3", null ]
];