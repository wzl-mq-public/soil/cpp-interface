var annotated_dup =
[
    [ "HTTP", "namespace_h_t_t_p.html", [
      [ "Resource", "class_h_t_t_p_1_1_resource.html", "class_h_t_t_p_1_1_resource" ],
      [ "Server", "class_h_t_t_p_1_1_server.html", "class_h_t_t_p_1_1_server" ]
    ] ],
    [ "MQTT", "namespace_m_q_t_t.html", [
      [ "Configuration", "class_m_q_t_t_1_1_configuration.html", "class_m_q_t_t_1_1_configuration" ],
      [ "Exception", "class_m_q_t_t_1_1_exception.html", "class_m_q_t_t_1_1_exception" ],
      [ "MessageContainer", "struct_m_q_t_t_1_1_message_container.html", "struct_m_q_t_t_1_1_message_container" ],
      [ "Publisher", "class_m_q_t_t_1_1_publisher.html", "class_m_q_t_t_1_1_publisher" ]
    ] ],
    [ "SIGN", "namespace_s_i_g_n.html", [
      [ "Hasher", "class_s_i_g_n_1_1_hasher.html", "class_s_i_g_n_1_1_hasher" ],
      [ "Signer", "class_s_i_g_n_1_1_signer.html", "class_s_i_g_n_1_1_signer" ]
    ] ],
    [ "SOIL", "namespace_s_o_i_l.html", [
      [ "Container", "class_s_o_i_l_1_1_container.html", "class_s_o_i_l_1_1_container" ],
      [ "Container< T, -1, -1 >", "class_s_o_i_l_1_1_container_3_01_t_00_01-1_00_01-1_01_4.html", "class_s_o_i_l_1_1_container_3_01_t_00_01-1_00_01-1_01_4" ],
      [ "Container< T, x, -1 >", "class_s_o_i_l_1_1_container_3_01_t_00_01x_00_01-1_01_4.html", "class_s_o_i_l_1_1_container_3_01_t_00_01x_00_01-1_01_4" ],
      [ "Element", "class_s_o_i_l_1_1_element.html", "class_s_o_i_l_1_1_element" ],
      [ "Enum", "class_s_o_i_l_1_1_enum.html", "class_s_o_i_l_1_1_enum" ],
      [ "Figure", "class_s_o_i_l_1_1_figure.html", "class_s_o_i_l_1_1_figure" ],
      [ "Function", "class_s_o_i_l_1_1_function.html", "class_s_o_i_l_1_1_function" ],
      [ "Object", "class_s_o_i_l_1_1_object.html", "class_s_o_i_l_1_1_object" ],
      [ "Parameter", "class_s_o_i_l_1_1_parameter.html", "class_s_o_i_l_1_1_parameter" ],
      [ "Range", "class_s_o_i_l_1_1_range.html", "class_s_o_i_l_1_1_range" ],
      [ "Range< ENUM >", "class_s_o_i_l_1_1_range_3_01_e_n_u_m_01_4.html", "class_s_o_i_l_1_1_range_3_01_e_n_u_m_01_4" ],
      [ "Range< std::string >", "class_s_o_i_l_1_1_range_3_01std_1_1string_01_4.html", "class_s_o_i_l_1_1_range_3_01std_1_1string_01_4" ],
      [ "Time", "class_s_o_i_l_1_1_time.html", "class_s_o_i_l_1_1_time" ],
      [ "Variable", "class_s_o_i_l_1_1_variable.html", "class_s_o_i_l_1_1_variable" ]
    ] ],
    [ "UDP", "namespace_u_d_p.html", [
      [ "Broadcast", "class_u_d_p_1_1_broadcast.html", "class_u_d_p_1_1_broadcast" ],
      [ "Configuration", "class_u_d_p_1_1_configuration.html", "class_u_d_p_1_1_configuration" ],
      [ "Exception", "class_u_d_p_1_1_exception.html", "class_u_d_p_1_1_exception" ]
    ] ]
];