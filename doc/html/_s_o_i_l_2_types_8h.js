var _s_o_i_l_2_types_8h =
[
    [ "BOOL", "_s_o_i_l_2_types_8h.html#a79f0d9e5821444f0265d9edf1e37f237", null ],
    [ "DIMENSION", "_s_o_i_l_2_types_8h.html#a738722655e044a443e338fde0f97e9a0", null ],
    [ "DOUBLE", "_s_o_i_l_2_types_8h.html#a1691ac525df63feb4f180fcc32f307f1", null ],
    [ "ENUM", "_s_o_i_l_2_types_8h.html#a1d5fe6676b3cd6aef4b24e9c4ba4f097", null ],
    [ "INT", "_s_o_i_l_2_types_8h.html#aef76c04a3939438dd7454193a3d56384", null ],
    [ "STRING", "_s_o_i_l_2_types_8h.html#a69374c360d593ed3cd4df6f3ebb40643", null ],
    [ "TIME", "_s_o_i_l_2_types_8h.html#a088b157f218c9c844ae77c2c07f1b328", null ]
];