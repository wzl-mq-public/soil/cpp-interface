var class_s_o_i_l_1_1_figure =
[
    [ "Figure", "class_s_o_i_l_1_1_figure.html#a8da920bd367b9a6ee8b70fcfa1f709ac", null ],
    [ "~Figure", "class_s_o_i_l_1_1_figure.html#a1f20334aa82abbb7d0e9d2322e5683d0", null ],
    [ "cast", "class_s_o_i_l_1_1_figure.html#a6cebd6af4116112bb244b1444aa3a1da", null ],
    [ "check_range", "class_s_o_i_l_1_1_figure.html#aaf9b99da1c712f54d758c492830a7566", null ],
    [ "operator*", "class_s_o_i_l_1_1_figure.html#abb3fe4e331a9a5f2f53b011309d6c937", null ],
    [ "operator=", "class_s_o_i_l_1_1_figure.html#a4e4d6c93a559d5e18178de9a79b472ec", null ],
    [ "read", "class_s_o_i_l_1_1_figure.html#a6dc0dda3c135fb3512486192a52e0f8b", null ],
    [ "set_range", "class_s_o_i_l_1_1_figure.html#af096fd3a66cdc35d18a1cbbf637dddf9", null ],
    [ "set_time", "class_s_o_i_l_1_1_figure.html#ab47f2268fb3f31960ab7bc8473974b3e", null ],
    [ "set_value", "class_s_o_i_l_1_1_figure.html#a358a1060f3f04ea4aa1cb0b7705af4d9", null ],
    [ "update", "class_s_o_i_l_1_1_figure.html#a65998054544cd71fdb47c0b1b956e883", null ],
    [ "wjson", "class_s_o_i_l_1_1_figure.html#aca02467b79b6d4a9590553b612876b20", null ],
    [ "write", "class_s_o_i_l_1_1_figure.html#af07e6f343402f0f9280ede65d4b26fa0", null ],
    [ "range", "class_s_o_i_l_1_1_figure.html#ab431dcf967cbfb8591b9128257595712", null ],
    [ "time", "class_s_o_i_l_1_1_figure.html#a8f320356ea837f06e439b74c2a22e81c", null ],
    [ "unit", "class_s_o_i_l_1_1_figure.html#a627d9d3386a719a6612ec3068812ea67", null ],
    [ "value", "class_s_o_i_l_1_1_figure.html#a97c7e2488708c155d366e2cdc1934c3b", null ]
];