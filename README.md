# SOIL C++
The **Unified Device Interface** provides four dynamic libraries which assemble a web-service for a sensor or measurement device.
The interface provides a RESTful HTTP server, a MQTT-Publisher, and UDP-Broadcaster, whereas the UDP-Broadcaster is of minor importance. 
The libary allows to fully implement a SOIL sensor service. By implementing your device against the provided API, 
it is possible to send message via MQTT which are then recieved by clients and to access the measurements and methods of the sensor/device via HTTP.

If a more detailed introduction or explanation of these concepts is requried, 
please contact [Benjamin Montavon](mailto:b.montavon@wzl-mq.rwth-aachen.de) or [Matthias Bodenbenner](mailto:m.bodenbenner@wzl-mq.rwth-aachen.de).
Same holds for any kind of problems when using, compiling, etc. the libraries.


## Documentation
The complete documentation of the interface can be found [here](https://wzl-mq-public.pages.git-ce.rwth-aachen.de/soil/cpp-interface/index.html).

## Development setup
### Dependencies
This library has the following dependencies (VCPKG naming):
- boost-date-time
- boost-asio
- boost-thread
- boost-algorithm
- nlohmann-json
- cpprestsdk
- paho-mqttpp3
- openssl

### Windows
1. Install Visual Studio - The most recent developments have been carried out with Visual Studio 2022.
The Community Edition is known to be sufficient and can be used free of charge for academic purposes. You can download it and see more details at [https://visualstudio.microsoft.com/de/vs/community/](https://visualstudio.microsoft.com/de/vs/community/).
2. Install VCPKG
    1. Clone the [vcpkg-repository](https://github.com/microsoft/vcpkg).
    2. Go into vcpkg folder.
    3. Run *.\bootstrap-vcpkg.bat*
    2. Configurure environment variables and path:
        1. Add environment variable ```VCPKG_INSTALLED``` and set the value to *[vcpkg-directory]/installed*. The Visual Studio project heavily depends on this path!
        2. Add the vcpkg directory to PATH.
    3. Do not forget to restart Visual Studio after changing the variables!
3. Install required packages. You can install **all** dependencies by executing *install_packages.bat*. It may take a lot of time (> 1h), have patience!. If you only want to use a specific library install the following packages. For compiling the x64 configuration install the packages and append *:x64-windows* (e.g. boost-date-time:x64-windows).
4. Compile source code.
5. The compiled *.dll-files and generated *.lib are copied into the *dist* folder under the chosen configuration (i.e. ./x64/Debug)
6. The *.dll files coming with the library still reside within ```VCPKG_INSTALLED``` within the respective subfolders (<x64/x86>-windows/[debug/]bin). Adding all folders to ```PATH``` may cause problems during debugging. It is recommended to put them next to the application or carefully select path modifications. During debugging, Visual Studio is managing this problem for you. 

### Linux
1. Install `git`, `gcc`, `make`, `cmake` at least and configure your build environment
2. Install VCPKG
    1. Clone the [vcpkg-repository](https://github.com/microsoft/vcpkg).
    2. Go into vcpkg folder.
    3. Run *./bootstrap-vcpkg.sh*
3. Install required packages. You may use *install_packages.sh*.
4. Run CMAKE using the vcpkg toolchain according to [https://vcpkg.readthedocs.io/en/latest/examples/installing-and-using-packages/#cmake](https://vcpkg.readthedocs.io/en/latest/examples/installing-and-using-packages/#cmake). When using the CMake GUI, use the option *Specify Toolchain for Cross-Compiling*, even if not Cross-Compiling. From the command-line, use `CMAKE_TOOLCHAIN_FILE`.
Using  `projects/linux/cmake/CMakeLists.txt` will build all components and test at once.


## Implement device conform to the unified interface
To implement a device service which is conform to the unified interface on has to inherit from the three classes *Object*, *Function* and *Variable*
of the **REST** project. The *main.cpp* of the *TEST* project can be used as template for main method of your device.

## Known Issues and Warnings
This project recently changed the default string backend for cpprestsdk from `std::wstring` to `std::string`. You may need to adjust some little pieces of code at your end when updating.
