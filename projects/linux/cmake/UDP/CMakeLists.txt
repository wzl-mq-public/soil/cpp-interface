cmake_minimum_required(VERSION 3.9)
project(UDP VERSION 1.0.0 DESCRIPTION "SOIL UDP")

set(PROJECT_ROOT "../../../../")

find_package(Boost REQUIRED COMPONENTS system thread)

add_library(UDP SHARED
    ${PROJECT_ROOT}/src/UDP/Broadcast.cpp
    ${PROJECT_ROOT}/src/UDP/Configuration.cpp
    ${PROJECT_ROOT}/src/UDP/Exception.cpp
)

target_link_libraries(UDP PRIVATE Boost::boost Boost::system Boost::thread) 

