cmake_minimum_required(VERSION 3.9)
project(SIGN VERSION 1.0.0 DESCRIPTION "SOIL SIGN")

set(PROJECT_ROOT "../../../../")

find_package(OpenSSL REQUIRED)

add_library(SIGN SHARED
    ${PROJECT_ROOT}/src/SIGN/Signer.cpp
    ${PROJECT_ROOT}/src/SIGN/Hasher.cpp
)

target_link_libraries(SIGN PRIVATE OpenSSL::SSL OpenSSL::Crypto)