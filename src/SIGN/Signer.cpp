#include "Signer.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <openssl/rsa.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/opensslv.h>

SIGN::Signer::Signer(std::string filename)
{
	_name = filename;
	std::ifstream keyfile(filename);
	std::stringstream buffer;
	buffer << keyfile.rdbuf();
	std::string key = buffer.str();

	BIO* bio = BIO_new(BIO_s_mem());
	BIO_write(bio, key.c_str(), static_cast<int>(key.length()));

	private_key = PEM_read_bio_PrivateKey(bio, NULL, NULL, NULL);

	BIO_free(bio);
	context = EVP_MD_CTX_create();
	EVP_MD_CTX_set_flags(context, EVP_MD_CTX_FLAG_ONESHOT & EVP_MD_CTX_FLAG_FINALISE & EVP_MD_CTX_FLAG_REUSE);
	

	// Make sure Signer gets a warm start
	std::vector<unsigned char> dummy(256, ' ');
	this->sign(dummy);

}


SIGN::Signer::~Signer()
{
	EVP_PKEY_free(private_key);
}

std::vector<unsigned char> SIGN::Signer::sign(std::vector<unsigned char> message)
{
	EVP_DigestSignInit(context, NULL, EVP_sha256(), NULL, private_key);
	EVP_DigestSignUpdate(context, message.data(), message.size());

	size_t length = 512;
	EVP_DigestSignFinal(context, NULL, &length);
	unsigned char* buffer = new unsigned char[length];
	EVP_DigestSignFinal(context, buffer, &length);
	//EVP_DigestSign(context, buffer, &length, message.data(), message.size());
	std::vector<unsigned char> result;
	result.reserve(length);
	for (unsigned int i = 0; i < length; i++)
	{
		result.push_back(buffer[i]);
	}
	delete [] buffer;
	return result;
}

std::string SIGN::Signer::openssl_version(void) const
{
	return OPENSSL_VERSION_TEXT;
}
