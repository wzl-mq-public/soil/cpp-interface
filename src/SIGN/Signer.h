#pragma once
#include "constants.h"
#include <vector>
#include <string>

typedef struct evp_pkey_st EVP_PKEY;
typedef struct evp_md_ctx_st EVP_MD_CTX;


namespace SIGN
{
	/**
	 * @brief Signer
	 * 
	 * Class to provide a convenient interface to sign bytestrings using a private key.
	 * Internally based on OpenSSL.
	*/
	class DLL Signer
	{
	private:
		/**
		 * @brief Internal Filename
		 * 
		 * Internal storage of the provided private key filename
		*/
		std::string _name;

		/**
		 * @brief Private Key
		 * 
		 * Internal pointer to the private key in OpenSSL format.
		*/

		EVP_PKEY* private_key;

		/**
		 * @brief OpenSSL Context
		 * 
		 * Internal pointer to the context object required by OpenSSL.
		*/
		EVP_MD_CTX* context;
	public:
		/**
		 * @brief Constructor
		 * 
		 * Default constructor loading the private key in PEM-format and initializing necessary methods.
		 * @param [in] filename Path to the private key in PEM-format.
		*/
		Signer(std::string filename);

		/**
		 * @brief Destructor
		 * 
		 * Destructor, internally calls EVP_PKEY_free.
		*/
		~Signer();

		/**
		 * @brief Sign bytes
		 * 
		 * Sign the bytestring provided to this function and returns the signature bytestring.
		 * @pre It is assumed that the actual conteht to be signed has already been hashed, e.g. using SHA256 and the Hasher class.
		 * 
		 * @param [in] digest Bytes to sign, typically a digest from a previous hashing function
		 * @return Signature as vector of bytes
		*/
		std::vector<unsigned char> sign(std::vector<unsigned char> digest);
		
		/**
		 * @brief OpenSSL Version
		 * 
		 * Retreive the OpenSSL Version string. This is helpful to check whether the correct OpenSSL library has been loaded as by experience on windows there are many OpenSSL versions.
		 * Using a too old version of OpenSSL leads to very bad performance.
		 * 
		 * @return OpenSSL Version String
		*/
		std::string openssl_version(void) const;

		/**
		 * @brief Filename
		 * 
		 * Function to retrieve the filename of the private key.
		 * 
		 * @return Filename of the private key.
		*/
		inline std::string name() const { return _name; }
	};
}

