#include "Hasher.h"
#include <openssl/evp.h>
#include <sstream>
#include <iomanip>


std::vector<unsigned char> SIGN::Hasher::sha256(const unsigned char* data, size_t length)
{
	EVP_MD_CTX* context = EVP_MD_CTX_new();
	EVP_DigestInit_ex(context, EVP_sha256(), NULL);
	EVP_DigestUpdate(context, data, length);
	std::vector<unsigned char> buffer(EVP_MD_size(EVP_sha256()));
	EVP_DigestFinal_ex(context, buffer.data(), NULL);
	EVP_MD_CTX_free(context);
	return buffer;
}

SIGN::Hasher::Hasher()
{
}


SIGN::Hasher::~Hasher()
{
}

std::vector<unsigned char> SIGN::Hasher::hash()
{
	return sha256(data.data(), data.size());
}

void SIGN::Hasher::reset()
{
	data.clear();
}

size_t SIGN::Hasher::size(void)
{
	return EVP_MD_size(EVP_sha256());
}

std::string SIGN::Hasher::print(std::vector<unsigned char> bytes, bool uppercase)
{
	std::ostringstream result;

	for (std::string::size_type i = 0; i < bytes.size();i++)
	{
		result << std::hex << std::setfill('0') << std::setw(2) << (uppercase ? std::uppercase : std::nouppercase) << (int)bytes[i];
		if (i != bytes.size() - 1)
		{
			result << std::setw(1) << " ";
		}
	}
		

	return result.str();
}
