#pragma once
#include <string>
#include "constants.h"

namespace MQTT
{
	/**
	 * @brief MQTT publishing configuration.
	 * 
	 * Class acting as enhanced struct to accomodate all configuration options.
	 * Therefore all members are public to be directly accessible.
	*/
	class DLL Configuration
	{
	public:
		/**
		 * @brief Hostname of the MQTT broker
		 * 
		 * Adress or hostname of the MQTT broker. Can be an FQDN or IP address.
		 * Defaults to @c 127.0.0.1 .
		*/
		std::string host;

		/**
		 * @brief Port of the MQTT broker
		 * 
		 * Port to use for the MQTT broker.
		 * Defaults to @c 1883 in the constructor. Typical ports are 1883 (MQTT), 8883 (MQTT over TLS) and 443 (MQTT over secure websockets).
		*/
		int port;

		/**
		 * @brief Username for connecting to the MQTT broker
		 * 
		 * Username when connecting to the MQTT broker.
		 * When using RabbitMQ, make sure to consider the VHOST prefix.
		 * Defaults to @c guest .
		*/
		std::string username;

		/**
		 * @brief Password for connecting to the MQTT broker
		 * 
		 * Password when connecting to the MQTT broker. Be mindful with passwords when committing to version control!
		 * Defaults to @c guest .
		*/
		std::string password;

		/**
		 * @brief Clean session flag
		 * 
		 * Flag whether to start a clean MQTT session.
		 * Defaults to @c true .
		*/
		bool clean_session;

		/**
		 * @brief  MQTT root topic
		 * 
		 * Root topic that is prepended to any published topic.
		 * This is intended for cases where permisions enfore that you publish under a defined root topic. 
		 * The default is no root topic prefix.
		*/
		std::string root;

		/**
		 * @brief Keep alive interval in seconds
		 * 
		 * Keep alive interval in seconds for the MQTT client.
		 * Defaults to @c 30 .
		*/
		int keep_alive;

		/**
		 * @brief Minimum delay between to messages in milliseconds
		 * 
		 * A minimum delay which should be kept between sending two messages, provided in milliseconds.
		 * Defaults to @c 0 .
		 * @post This is not enforced in the client, but should be called as property of the publishing by the using code.
		*/
		int min_delay_ms;
		
		/**
		 * @brief Connection timeout in seconds
		 * 
		 * Number of seconds to wait until the connection succeeds. 
		 * This influence the length of the blocking call during connection.
		 * Defaults to @c 30 .
		*/
		int connection_timeout_s;

		/**
		 * @brief Use secured connection
		 * 
		 * Boolean flag whether to use a secured connection (MQTT over TLS or secure websocket).
		 * Defaults to @c false . 
		 * @pre If not ignoring verification, trusted root certificated must be provided in a PEM-file.
		*/
		bool ssl;

		/**
		 * @brief Skip SSL verification
		 * 
		 * Boolean flag whether to ignore SSL certificate validation, i.e. for hostname matching and trusted authority.
		 * This should only be used for basic development purposes.
		*/
		bool verify;

		/**
		 * @brief Use websocket protocol
		 * 
		 * Boolean flag whether to use the websocket extension of MQTT.
		 * Make sure to specify the correct path.
		 * Defaults to @c false .
		*/
		bool websocket;

		/**
		 * @brief Websocket path
		 * 
		 * Websocket path that forms the last part of the URI, e.g. @c /mqtt-ws/.
		 * Defaults to @c "" .
		*/
		std::string path;

		/**
		 * @brief Path to CA PEM-file
		 * 
		 * Path to a file containing the trusted certificate authorities in PEM-format for OpenSSL.
		 * This is mandatory when properly implementing secure MQTT.
		 * @pre Make sure that the hostname used for connecting is matching its name in the certificate chain.
		*/
		std::string certificate_authority;
		
		/**
		 * @brief Default constructor
		 * 
		 * Default constructor applying the defaults documented. 
		*/
		Configuration();

		/**
		 * @brief JSON Constructor
		 * 
		 * Reads the configuration from a JSON-file.
		 * See the project assets for an example file.
		 * @param [in] filename Path to JSON-file (absolute or relative).
		*/
		Configuration(std::string filename);

		/**
		* @brief Destructor
		* 
		* Standard Destructor
		*/
		~Configuration();

		/**
		 * @brief URI Builder
		 * 
		 * Builds an URI for the underlying Paho-MQTT library.
		 * @return Full URI string, e.g. @c tcp://127.0.0.1:1883 .
		*/
		std::string uri();
	};
}