#include "Configuration.h"
#include <nlohmann/json.hpp>
#include <fstream>


using json = nlohmann::json;

MQTT::Configuration::Configuration()
{
	std::string host = "127.0.0.1";
	port = 1883;
	username = "guest";
	password = "guest";
	clean_session = true;
	root = "";
	keep_alive = 30;
	min_delay_ms = 0;
	int connection_timeout_s = 30;
	bool ssl = false;
	bool verify = true;
	bool websocket = false;
	std::string path = "";
	std::string certificate_authority = "";
	connection_timeout_s = 30;
}

MQTT::Configuration::Configuration(std::string filename)
{
	std::ifstream infile(filename);
	json j = json::parse(infile);
	host = j.value("host", "127.0.0.1");
	port = j.value("port", 1883);
	username = j.value("username", "guest");
	password = j.value("password", "guest");
	clean_session = j.value("clean", true);
	root = j.value("root", "");
	keep_alive = j.value("keep_alive", 30);
	min_delay_ms = j.value("min_delay", 0);
	connection_timeout_s = j.value("connection_timeout", 30);
	ssl = j.value("ssl", false);
	verify = j.value("verify", true);
	websocket = j.value("websocket", false);
	path = j.value("path", "");
	certificate_authority = j.value("certificate_authority", "");

}

MQTT::Configuration::~Configuration()
{
}

std::string MQTT::Configuration::uri()
{
	std::string prefix;
	if (websocket)
	{
		if (ssl)
		{
			prefix = "wss";
		}
		else
		{
			prefix = "ws";
		}
		if (path.size() > 0)
		{
			if (path.at(0) != '/')
			{
				path = "/" + path;
			}
		}
	}
	else
	{
		if (ssl)
		{
			prefix = "ssl";
		}
		else
		{
			prefix = "tcp";
		}
		path = "";
	}
	
	return prefix + "://" + host + ":" + std::to_string(port) + path;

}

