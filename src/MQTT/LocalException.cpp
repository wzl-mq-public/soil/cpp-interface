#include "LocalException.h"



MQTT::Exception::Exception(const char* message, int code): std::runtime_error(message), _code(code)
{
}

MQTT::Exception::Exception(const std::exception & exc) : std::runtime_error(exc.what())
{
	_code = 0;
}

MQTT::Exception::~Exception()
{
}


