#pragma once
#include "constants.h"
#include <stdexcept>


namespace MQTT
{
	/**
	 * @brief MQTT Publisher Exception
	 * 
	 * Specific subclass of @c std::runtime_error to distinguish exceptions coming from the MQTT Publisher.
	*/
	class DLL Exception : public std::runtime_error
	{
	private:

		/**
		 * @brief MQTT Result Code
		 * 
		 * MQTT has an own result code system which is useful to describe errors and therefore included here
		*/
		int _code;
	public:

		/**
		 * @brief Constructor
		 * 
		 * Constructor building an expection from a message and result code.
		 * @param [in] message Message to use for exeption description
		 * @param [in] code result code to store.
		*/
		Exception(const char* message = "", int code = 0);

		/**
		 * @brief Construcor from exception
		 * 
		 * Constructor to rethrow an exception that has occured from some other error within the context of the MQTT Publisher.
		 * @param [in] exc Expcetion to replicate 
		*/
		Exception(const std::exception& exc);

		/**
		 * @brief Destructor
		 * 
		 * Standard destructor for the class
		*/
		~Exception(void);

		/**
		 * @brief Result Code
		 * 
		 * Get the result code that is stored internally in the exception.
		 * @return result code. 
		*/
		inline const int code(void) const { return _code; }
	};
}



