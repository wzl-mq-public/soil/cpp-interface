#pragma once
#include "constants.h"
namespace MQTT
{
	/**
	 * @brief Internal MQTT message struct
	 * 
	 * Struct to manage MQTT message data internally using standard strings.
	*/
	struct DLL  MessageContainer
	{
		/**
		 * @brief Message Topic
		 * 
		 * Topic to which the message shall be sent.
		*/
		std::string topic;

		/**
		 * @brief Message
		 * 
		 * Primary content of the messsage to send.
		*/
		std::string message;

		/**
		 * @brief Quality of Service
		 * 
		 * MQTT Quality of service level to choose for the message.
		 * Check the MQTT specifications for the exact behaviours of 0, 1, and 2 in conjuncation with message retention.
		*/
		int qos;

		/**
		 * @brief Retain Flag
		 * 
		 * Flag whether to retain the message on the broker after disconnection.
		*/
		bool retain;

	};
}