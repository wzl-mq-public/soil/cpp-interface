#include "Range.h"

SOIL::Range<std::string>::Range() : set(false), low(0), high(std::numeric_limits<size_t>::max())
{
}

SOIL::Range<std::string>::Range(size_t low, size_t high) : set(true), low(low), high(high)
{
}

SOIL::Range<std::string>::Range(std::vector<size_t> limits) : set(true)
{
	if (limits.size() == 0)
	{
		set = false;
	}
	else if (limits.size() == 2)
	{
		low = limits.at(0);
		high = limits.at(1);
		set = true;
	}
	else
	{
		throw std::runtime_error("Invalid vector for range initialization!");
	}
}

SOIL::Range<std::string>::~Range()
{
}

web::json::value SOIL::Range<std::string>::wjson(void)
{
	web::json::value range_array = web::json::value::array();
	if (set)
	{
		range_array[0] = SOIL::to_json<int>(static_cast<int>(low));
		range_array[1] = SOIL::to_json<int>(static_cast<int>(high));
	}
	return range_array;
}

bool SOIL::Range<std::string>::check(const std::string & value)
{
	if (set)
	{
		return ((value.size() >= low) && (value.size() <= high));
	}
	else
	{
		return true;
	}
}



SOIL::Range<SOIL::ENUM>::Range() : set(false)
{
}


SOIL::Range<SOIL::ENUM>::Range(std::vector<std::string> choices) : set(true), choices(choices)
{
	
}

SOIL::Range<SOIL::ENUM>::~Range()
{
}

web::json::value SOIL::Range<SOIL::ENUM>::wjson(void)
{
	web::json::value range_array = web::json::value::array();
	if (set)
	{
		for (int i = 0; i < static_cast<int>(choices.size()); i++)
		{
			range_array[i] = SOIL::to_json<std::string>(choices.at(i));
		}
	}
	return range_array;
}

bool SOIL::Range<SOIL::ENUM>::check(const std::string& value)
{
	if (set)
	{ 
		for (int i = 0; i < static_cast<int>(choices.size()); i++)
		{
			if (value == choices.at(i))
			{
				return true;
			}
		}
		return false;
	}
	else
	{
		return true;
	}
}

bool SOIL::Range<SOIL::ENUM>::check(const SOIL::ENUM& value)
{
	return this->check(value.selected());
}

