#pragma once
#include "constants.h"
#include <string>
#include <vector>
#include <stdexcept>

namespace SOIL
{
	/**
	 * @brief SOIL Enum Datatype
	 *
	 * C++ class to represent the SOIL Enum datatype.
	 * Internally it is based on strings.
	*/
	class DLL Enum
	{
	private:
		/**
		 * @brief Selected value
		 *
		 * Currently selected value as string.
		*/
		std::string _selected;

		/**
		 * @brief Enum Choices
		 *
		 * Vector of strings representing the different values the enumeration can take and is constrained to.
		*/
		std::vector<std::string> _choices;
	public:
		/**
		 * @brief Constructor
		 *
		 * Standard constructor which leaves everything uninitialized.
		*/
		Enum();

		/**
		 * @brief Constructor intialized with value
		 *
		 * Constructor that initializes the enumeration with a selected value.
		 *
		 * @todo Check whether this constructor is still meaningful in future releases.
		 * Usage is not recommended.
		 * @param [in] value Value for initialization
		*/
		Enum(std::string value);

		/**
		 * @brief Initializing Container
		 *
		 * Constructor that initializes the enumeration with choices and a value.
		 *
		 * @param [in] value Value for initialization
		 * @param [in] choices Applicable set of choices for the enumeration
		*/
		Enum(std::string value, std::vector<std::string> choices);

		/**
		 * @brief Destructor
		 *
		 * Standard destructor without special efforts.
		*/
		~Enum();

		/**
		 * @brief Get selected value
		 *
		 * Get the value that the enum currently holds as selected.
		 * @return Current value
		*/
		std::string selected(void) const;

		/**
		 * @brief Get Choices
		 *
		 * Get the choices that are available for this enumeration.
		 * @return Available choices as vector of strings
		*/
		std::vector<std::string> choices(void);

		/**
		 * @brief Get index of selected element
		 *
		 * Traversed the vector of choices and return the index of the current element.
		 * @return Index of the selected element
		*/
		int index() const;

		/**
		 * @brief Determine index of value
		 *
		 * Converts a string value to its integer value.
		 * This function is useful when interacting with C++ ENUMs consisting of integers.
		 *
		 * @exception std::logic_error If the value is not found, an exception is thrown.
		 *
		 * @param [in] value Value to search
		 * @return Index of the searched value
		*/
		int index(std::string value) const;

		/**
		 * @brief Set selected item (index)
		 *
		 * Set the currently selected item the enumeration holds based on its integer index.
		 *
		 * @exception std::logic_error If the index exceeds the number of choices, an exception is thrown.
		 *
		 * @param [in] value Index of the value to set
		*/
		void set(int value);

		/**
		 * @brief Set selected item (value)
		 *
		 * Set the currently selected item the enumeration holds based on a string value.
		 *
		 * @exception std::logic_error If the value is not among the choices, an exception is thrown.
		 *
		 * @param [in] value Value to set
		*/
		void set(std::string value);
	};
}