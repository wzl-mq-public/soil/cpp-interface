#include "Enum.h"


SOIL::Enum::Enum()
{
}

SOIL::Enum::Enum(std::string value) : _selected(value)
{
}

SOIL::Enum::Enum(std::string value, std::vector<std::string> choices): _selected(value), _choices(choices)
{
}


SOIL::Enum::~Enum()
{
}

std::string SOIL::Enum::selected(void) const
{
	return _selected;
}

std::vector<std::string> SOIL::Enum::choices(void)
{
	return _choices;
}

int SOIL::Enum::index() const
{
	for (int i = 0; i < static_cast<int>(_choices.size()); i++)
	{
		if (_selected == _choices.at(i))
		{
			return i;
		}
	}
	throw std::logic_error("Value of ENUM not in choices!");
}

int SOIL::Enum::index(std::string value) const
{
	for (int i = 0; i < static_cast<int>(_choices.size()); i++)
	{
		if (value == _choices.at(i))
		{
			return i;
		}
	}
	throw std::logic_error("Value of ENUM not in choices!");
}

void SOIL::Enum::set(int value)
{
	if (value < static_cast<int>(_choices.size()))
	{
		_selected = _choices.at(value);
	}
	else
	{
		throw std::logic_error("Index not in ENUM choices!");
	}
}

void SOIL::Enum::set(std::string value)
{
	_selected = value;
}
