#include "Object.h"


SOIL::Object::Object(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology) : Element(parent, uuid, name, description, ontology)
{
	if (uuid.substr(0, 3) != "OBJ")
	{
		throw std::logic_error("UUIDs for objects must start with OBJ!");
	}
	allowed_methods = { HTTP::Methods::GET, HTTP::Methods::OPTIONS, HTTP::Methods::HEAD, HTTP::Methods::PUT };
}


SOIL::Object::~Object()
{
}

std::shared_ptr<SOIL::Object> SOIL::Object::create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology)
{
	Object* object = new Object(parent, uuid, name, description, ontology);
	return object->ptr();
}

HTTP::Json SOIL::Object::wjson(void)
{
	
	HTTP::Json json_root = Element::wjson();
	json_root[_XPLATSTR("children")] = HTTP::Json::array();
	int i = 0;
	for (auto& child : children)
	{
		json_root[_XPLATSTR("children")][i] = HTTP::Json();
		json_root[_XPLATSTR("children")][i][_XPLATSTR("uuid")] = HTTP::Json::string(utility::conversions::to_string_t(child.first));
		json_root[_XPLATSTR("children")][i][_XPLATSTR("name")] = HTTP::Json::string(utility::conversions::to_string_t(child.second->name));
		i++;
	}
	return json_root;
}

void SOIL::Object::read(void)
{
}

HTTP::Json SOIL::Object::insert(HTTP::Json body)
{
	throw std::logic_error("This function has not been implemented!");
}

void SOIL::Object::remove(void)
{

}

HTTP::Response SOIL::Object::handle_get(HTTP::Request message, std::smatch match)
{
	this->read();
	HTTP::Response response;
	response.set_body(this->wjson());
	response.set_status_code(HTTP::Status::OK);
	return response;
}

HTTP::Response SOIL::Object::handle_put(HTTP::Request message, std::smatch match)
{
	auto task = message.extract_json();
	task.wait();
	HTTP::Json body  = task.get();
	HTTP::Json json = this->insert(body);
	HTTP::Response response;
	response.set_body(json);
	response.set_status_code(HTTP::Status::Created);
	return response;
}

HTTP::Response SOIL::Object::handle_delete(HTTP::Request message, std::smatch match)
{
	this->remove();
	parent->remove(this->uuid);
	HTTP::Response response;
	response.set_status_code(HTTP::Status::OK);
	return response;
}
