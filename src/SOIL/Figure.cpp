#include "Figure.h"

template<>
HTTP::Json SOIL::datatype<double>(void) 
{
	return HTTP::Json::string(_XPLATSTR("double"));
}

template<>
HTTP::Json SOIL::datatype<bool>(void)
{
	return HTTP::Json::string(_XPLATSTR("bool"));
}


template<>
HTTP::Json SOIL::datatype<std::string>(void)
{
	return HTTP::Json::string(_XPLATSTR("string"));
}


template<>
HTTP::Json SOIL::datatype<int64_t>(void)
{
	return HTTP::Json::string(_XPLATSTR("int"));
}


template<>
HTTP::Json SOIL::datatype<int>(void)
{
	return HTTP::Json::string(_XPLATSTR("int"));
}

template<>
HTTP::Json SOIL::datatype<SOIL::TIME>(void)
{
	return HTTP::Json::string(_XPLATSTR("time"));
}

template<>
HTTP::Json SOIL::datatype<SOIL::ENUM>(void)
{
	return HTTP::Json::string(_XPLATSTR("enum"));
}