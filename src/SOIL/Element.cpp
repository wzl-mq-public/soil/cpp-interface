#include "Element.h"
#include "json_helpers.h"


SOIL::Element::Element(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology) : parent(parent), uuid(uuid), name(name), description(description), ontology(ontology)
{
	if (parent.use_count() > 0)
	{
		parent->add(uuid, this);
		self = (*parent)[uuid];
	}
	else
	{
		self.reset(this, &SOIL::null_deleter);
	}
}


SOIL::Element::~Element()
{
	children.clear();
}

#pragma warning( push )
#pragma warning( disable : 4717)
std::vector<std::string> SOIL::Element::fqid(void)
{
	std::vector<std::string> parent_fqid;
	if (parent.use_count() > 0)
	{
		parent_fqid = parent->fqid();
	}
	parent_fqid.push_back(uuid);
	return parent_fqid;
}
#pragma warning(pop)

std::shared_ptr<SOIL::Element> SOIL::Element::operator[](std::string fqid)
{
	if (fqid == "" || fqid == "/")
	{
		return self;
	}
	size_t path_length = fqid.size();
	if (fqid[0] == '/')
	{
		fqid = fqid.substr(1, path_length-1);
		path_length--;
	}
	size_t first_delimiter = fqid.find("/");
	if (first_delimiter == std::string::npos)
	{
		if (children.count(fqid) == 0)
		{
			if (this->uuid == fqid)
			{
				return self;
			}
			throw std::runtime_error(fqid + " does not match any children of " + uuid);
		}
		return children[fqid];
	}
	else
	{
		std::string dock_off_path = fqid.substr(0, first_delimiter);
		std::string continue_path = fqid.substr(first_delimiter + 1, path_length - first_delimiter - 1);
		if (children.count(dock_off_path) == 0)
		{
			if (this->uuid == dock_off_path)
			{
				return (*self)[continue_path];
			}
			throw std::runtime_error(dock_off_path + " does not match any children of " + uuid);
		}
		return (*children[dock_off_path])[continue_path];
	}
}

std::shared_ptr<SOIL::Element> SOIL::Element::add(std::string uuid, std::shared_ptr<Element> child)
{
	bool exists = children.count(uuid) > 0;
	children[uuid] = child;
	return child;
}

std::shared_ptr<SOIL::Element>  SOIL::Element::add(std::string uuid, Element * child)
{
	return add(uuid, std::shared_ptr<Element>(child));
}

bool SOIL::Element::insert(std::string uuid, std::shared_ptr<Element> child)
{
	bool exists = children.count(uuid) > 0;
	this->add(uuid, child);
	return exists;
}

bool SOIL::Element::insert(std::string uuid, Element* child)
{
	return insert(uuid, std::shared_ptr<Element>(child));
}

bool SOIL::Element::remove(std::string uuid)
{
	bool exists = children.count(uuid) > 0;
	children.erase(uuid);
	return exists;
}

bool SOIL::Element::is_object(void) const
{
	return (uuid.substr(0, 3) == "OBJ");
}

bool SOIL::Element::is_variable(void) const
{
	return (uuid.substr(0, 3) == "VAR");
}

bool SOIL::Element::is_function(void) const
{
	return (uuid.substr(0, 3) == "FUN");
}

bool SOIL::Element::is_parameter(void) const
{
	return (uuid.substr(0, 3) == "PAR");
}

HTTP::Json SOIL::Element::wjson(void)
{
	HTTP::Json json_root;
	json_root[_XPLATSTR("uuid")] = SOIL::to_json(uuid);
	json_root[_XPLATSTR("name")] = SOIL::to_json(name);
	json_root[_XPLATSTR("description")] = SOIL::to_json(description);
	if (ontology == "")
	{
		json_root[_XPLATSTR("ontology")] = HTTP::Json::null();
	}
	else
	{
		json_root[_XPLATSTR("ontology")] = SOIL::to_json(ontology);
	}

	return json_root;
}

std::string SOIL::Element::json(void)
{
	HTTP::Json content = wjson();
	return utility::conversions::to_utf8string(content.serialize());
}


HTTP::Response SOIL::Element::handle(HTTP::Request request, std::smatch match)
{
	try
	{
		std::string path = "";
		if (match.size() > 0)
		{
			path = match.str(0);//utility::conversions::to_utf8string(match.str(0));
		}

		size_t query_delimiter = path.find("?");
		std::shared_ptr<Element> resource = (*self)[path.substr(0, query_delimiter)];

		if (resource == self)
		{
			return Resource::handle(request, match);
		}
		else
		{
			return resource->Resource::handle(request, match);
		}

	}
	catch (std::exception& exception)
	{
		return Resource::handle_exception(request, exception, match);

	}
	
}
