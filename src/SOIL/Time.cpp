#include "Time.h"
#include <boost/algorithm/string/replace.hpp>



std::string SOIL::Time::rfc3339(void) const
{
	if (is_null())
	{
		return "";
	}
	else
	{
		std::string timestring = boost::posix_time::to_iso_extended_string(timestamp);
		boost::replace_all(timestring, ",", ".");
		if (timestring == "not-a-date-time")
		{
			return "";
		}
		else
		{
			return timestring + "Z";
		}
	}
}

SOIL::Time::Time()
{
}

SOIL::Time::Time(std::string value)
{
	if (value == "")
	{
		_null = true;
	}
	else
	{
		boost::replace_all(value, "T", " ");
		boost::replace_all(value, "Z", "");
		timestamp = boost::posix_time::time_from_string(value);
		_null = false;
	}
}

SOIL::Time::Time(boost::posix_time::ptime value)
{
	_null = false;
	timestamp = value;
}


SOIL::Time::~Time()
{
}

boost::posix_time::ptime SOIL::Time::utc_now(void)
{
	 return boost::posix_time::microsec_clock::universal_time();
}

std::vector<unsigned char> SOIL::Time::serialize(void) const
{
	if (is_null())
	{
		throw std::logic_error("Time is not set");
	}
	std::vector<unsigned char> serialization;
	size_t size = sizeof(uint16_t) + 5 * sizeof(uint8_t) + sizeof(uint32_t);
	serialization.reserve(size);

	auto date = timestamp.date();
	auto time = timestamp.time_of_day();
	uint16_t year = static_cast<uint16_t>(date.year());
	uint8_t month = static_cast<uint8_t>(date.month().as_number());
	uint8_t day = static_cast<uint8_t>(date.day().as_number());
	uint8_t hour = static_cast<uint8_t>(time.hours());
	uint8_t minute = static_cast<uint8_t>(time.minutes());
	uint8_t seconds = static_cast<uint8_t>(time.seconds());
	uint32_t nanoseconds = static_cast<uint32_t>(time.fractional_seconds());

	switch (time.num_fractional_digits())
	{
	case 3:
		nanoseconds *= 1000000;
		break;
	case 6:
		nanoseconds *= 1000;
		break;
	case 9:
		break;
	default:
		throw std::logic_error("Unknown fractional seconds!");
	}

	unsigned char* pointer = reinterpret_cast<unsigned char*>(&year);
	for (int i = 0; i < sizeof(uint16_t); i++)
	{
		serialization.push_back(pointer[i]);
	};
	serialization.push_back(month);
	serialization.push_back(day);
	serialization.push_back(hour);
	serialization.push_back(minute);
	serialization.push_back(seconds);

	pointer = reinterpret_cast<unsigned char*>(&nanoseconds);
	for (int i = 0; i < sizeof(uint32_t); i++)
	{
		serialization.push_back(pointer[i]);
	};

	return serialization;
}

bool SOIL::operator>=(const SOIL::Time & t1, const SOIL::Time & t2)
{
	if (t1.is_null() || t2.is_null())
	{
		return false;
	}
	else
	{
		return (t1.timestamp >= t2.timestamp);
	}
}

bool SOIL::operator<=(const SOIL::Time & t1, const SOIL::Time & t2)
{
	if (t1.is_null() || t2.is_null())
	{
		return false;
	}
	else
	{
		return (t1.timestamp <= t2.timestamp);
	}
}
