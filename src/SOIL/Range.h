#pragma once
#include "constants.h"
#include "json_helpers.h"
#include "Types.h"
#include "REST/Types.h"

namespace SOIL
{
	/**
	 * @brief Range Helper Class
	 * 
	 * Class to represent the acceptable range of a figure. This class is templated to be applicable to all types.
	 * For numerical values, the range indicates a lower and upper limit which may be validly assigned. The limit values are included.
	 * The same holds for times. For strings, it indicates the minimum and maximum length.
	 * For enumerations, this is a list of applicable choices.
	 * For these types, the template is explicitly implemented differently.
	 * The range can be explicitly unset to provide an easy default member implementation.
	 * 
	 * There is no set function, if the range changes, a new instance of this lightweight class should be created.
	 *
	 * @tparam T Datatype for which the range is defined
	*/
	template <typename T>
	class Range
	{
	private:
		/**
		 * @brief Lower Limit
		 * 
		 * Lower limit against which the range is checked.
		*/
		T low;

		/**
		 * @brief Upper Limit
		 * 
		 * Upper limit against which the range is checked.
		*/
		T high;

		/**
		 * @brief Set flag
		 * 
		 * Boolean flag whether limits are actually set. If false,
		 * no range restriction is in place.
		*/
		bool set;
	public:
		/**
		 * @brief Default Constructor
		 * 
		 * Default Constructor, sets no limits and initializes @c set to false.
		*/
		Range();

		/**
		 * @brief Argument constructor
		 * 
		 * Constructs the range and initialized lower and upper limit with the provided values.
		 * 
		 * @param [in] low Lower limit 
		 * @param [in] high Upper limit
		*/
		Range(T low, T high);

		/**
		 * @brief List constructor
		 *
		 * Constructs the range and initialized lower and upper limit with the provided vector.
		 * This is constructor can be conveniently initialized with the list notation {low, high}.
		 *
		 * @param [in] limits Vector of length 2 for initialization
		*/
		Range(std::vector<T> limits);

		/**
		 * @brief Destructor
		 *
		 * Standard desctructor, no custom implementation.
		*/
		~Range();

		/**
		 * @brief HTTP JSON
		 * 
		 * Returns the SOIL conformant partial representation of the range of Figure, i.e. @em null if not set. 
		 * @return JSON Object
		*/
		HTTP::Json wjson(void);

		/**
		 * @brief Check
		 * 
		 * Check whether a given value is inside the prescribed range or not.
		 * If @c set is false, it always returns to true.
		 * The limits are included in the valid range, i.e. it is checked with `<=` and `>=`. 
		 * 
		 * @param [in] value Value to check
		 * @return Check result als boolean, i.e. true if in range 
		*/
		bool check(const T & value);
	};

	/**
	 * @brief String Range
	 * 
	 * Specialization of Range for strings, where the limits correspond to minimal and maximum length.
	*/
	template<>
	class DLL Range<std::string>
	{
	private:
		size_t low;
		size_t high;
		bool set;
	public:
		Range();
		Range(size_t low, size_t high);
		Range(std::vector<size_t> limits);
		~Range();
		HTTP::Json wjson(void);
		bool check(const std::string& value);
	};

	/**
	 * @brief Enum Range
	 *
	 * Specialization of Range for enumerations, where the limit is a set of applicable choices.
	*/
	template<>
	class DLL Range<ENUM>
	{
	private:
		std::vector<std::string> choices;
		bool set;
	public:
		Range();
		Range(std::vector<std::string> choices);
		~Range();
		HTTP::Json wjson(void);
		bool check(const std::string& value);
		bool check(const SOIL::ENUM& value);
	};

	template<typename T>
	Range<T>::Range() : set(false)
	{
	}

	template<typename T>
	Range<T>::Range(T low, T high): set(true), low(low), high(high)
	{
	}

	template<typename T>
	Range<T>::Range(std::vector<T> limits)
	{
		if (limits.size() == 0)
		{
			set = false;
		}
		else if (limits.size() == 2)
		{
			low = limits.at(0);
			high = limits.at(1);
			set = true;
		}
		else
		{
			throw std::runtime_error("Invalid vector for range initialization!");
		}
	}

	template<typename T>
	Range<T>::~Range()
	{
	}

	template<typename T>
	HTTP::Json Range<T>::wjson(void)
	{
		HTTP::Json range_array = HTTP::Json::array();
		if (set)
		{
			range_array[0] = to_json<T>(low);
			range_array[1] = to_json<T>(high);
		}
		return range_array;
	}


	template<typename T>
	bool Range<T>::check(const T & value)
	{
		if (set)
		{
			return ((value >= low) && (value <= high));
		}
		else
		{
			return true;
		}
	}


	

}


