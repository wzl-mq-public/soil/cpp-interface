#pragma once
#include "constants.h"
#include "Types.h"
#include "Element.h"
#include "Container.h"
#include "Range.h"
#include "Figure.h"
#include "MQTT/Publisher.h"
#include <boost/algorithm/string/join.hpp>

namespace SOIL
{
	/**
	 * @brief Parameter Class
	 * 
	 * This class represents a SOIL Parameter. Parameter and Variable share many common properties
	 * and therefore both inherit from Figure, such that methods there should be considered in any case.
	 * Parameters are primarily intended for values that do not represent any measurement or physical phenomenon outside the immediate control of device.
	 * In contrast to variables, they can be set externally to control the device and do not posses a measurement uncertainty. Parametery may be constant.
	 * In lightweight scenarios, this class may be instantiated directly, for more specific scenarios, it shouls be subclassed and override the implementations
	 * of `read()` and `write()`.
	 * 
	 * A parameter supports HTTP GET (read) and HTTP PATCH (set) verbs.
	 * 
	 * The data management of Figure and hence Parameter relies on Container, such that the same templating logic is used.
	 *
	 * @tparam T Type of the data.
	 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
	 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
	 * 
	 * @todo The HTTP handlers may be moved to protected if HTTP::Server is declared as friend class. Currently this is not done to alllow for greater flexibility. 
	*/
	template <typename T, int x=-1, int y=-1>
	class Parameter : public Figure<T, x, y>
	{
	protected:
		
		//Looks stale as Figure as a time member
		//TIME time;

		/**
		 * @brief Constant flag
		 * 
		 * Boolean flag whether this parameter should be considered as constant.
		*/
		bool constant;

		/**
		 * @brief Read callback
		 *
		 * Read callback that can be implemented by deriving classes to perform custom build logic on read actions, e.g. update the value from an external storage.
		 * Declared virtual to make sure the derived method is called first. Does nothing be default.
		*/
		virtual void read(void);

		/**
		 * @brief Write callback
		 *
		 * Write callback that can be implemented by deriving classes to perform custom build logic on write actions, e.g.set a value to an external system.
		 * Declared virtual to make sure the derived method is called first. Does nothing be default.
		*/
		virtual void write(void);

	public:
		/**
		 * @brief Constructor
		 *
		 * Standard constructor intialiazing the values. If subclassed, it should be called from the subclass constructor.
		 * @param [in] parent Shared pointer to parent object.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] constant Boolean flag whether this value is cosnstant
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 * @param [in] range Allowed range for the variable values, defaults to an empty object, i.e. allowing all values
		 * @param [in] time Timestamp for the initial value, defaults to unset
		*/
		Parameter(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, bool constant = false, std::string ontology = "", Range<T> range = Range<T>(), TIME time = TIME());
		
		/**
		 * @brief Destructor
		 *
		 * Default destructor, without custom effort.
		*/
		~Parameter();

		/**
		 * @brief Create new Parameter
		 *
		 * Create a new Parameter using the default constructor and return a shared pointer reference.
		 * This is the preferred method for manual creation with consistent lifecycle handling.
		 * @param [in] parent Shared pointer to parent object.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] constant Boolean flag whether this value is cosnstant
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 * @param [in] range Allowed range for the variable values, defaults to an empty object, i.e. allowing all values
		 * @param [in] time Timestamp for the initial value, defaults to unset
		*/
		static std::shared_ptr<Parameter> create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, bool constant = false, std::string ontology = "", Range<T> range = Range<T>(), TIME time = TIME());

		/**
		 * @brief Assignment operator
		 *
		 * Assigns the value provided as container on the right hand side to the parameter.
		 * Immediately resorts to the implementation in Figure internally.
		 *
		 * @exception std::range_error Throws an exception if the value to assign is outside the allowed range.
		 *
		 * @param[in] value Value to assign
		 * @return Reference to the current Parameter
		*/
		Parameter<T, x, y>& operator =(const Container<T, x, y>& value);


		/**
		 * @brief HTTP JSON
		 *
		 * Get a HTTP JSON object corresponding to the current state of the Parameter.
		 * This function provides a SOIL-conformant JSON representation the parameter. 
		 * It internally extends the method of Figure.
		 * 
		 * @return JSON object
		*/
		HTTP::Json wjson(void) override;

		/**
		 * @brief HTTP GET Handler
		 *
		 * Handler that is called by the server on HTTP requests on a GET method.
		 * This function returns a representation of the Parameter and its current value to the requesting party.
		 * It should not be overridden directly in subclasses, instead the `read()` function should be overriden.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_get(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief HTTP PATCH Handler
		 *
		 * Handler that is called by the server on HTTP requests on a PATCH method.
		 * This function updates the value of a parameter. The request must at least contain a 
		 * It should not be overridden directly in subclasses, instead the `write()` function should be overriden.
		 * 
		 * If an update is not not foreseen, you may consider overriding `allowed_methods` without PATCH.
		 * This is not enforced by default through the `constant` flag.
		 * The minimum requirement to the body is that it contains a value to assign. If a timestamp is provided, it will be set, otherwise the current server time is taken.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_patch(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief Get Pointer
		 *
		 * Return a shared pointer casted to the Parameter type to element itself.
		 * @return Casted pointer
		*/
		inline std::shared_ptr<Parameter> ptr(void)
		{
			return std::dynamic_pointer_cast<Parameter>(Element::self);
		}

		/**
		 * @brief Publish to MQTT
		 * 
		 * Publish the current JSON representation under the FQID as topic using a given MQTT publisher.
		 * This function must be explicitly called from the user's code as otherwise the update cycle would
		 * depend on the publisher and the user will be left without control to call other methods before publishing.
		 *
		 * A good pattern is to have a reference to an MQTT publisher in a sublcassing implementation and implement a complete update
		 * cycle there.
		 * 
		 * @pre An MQTT::Publisher must be instantiated elsewhere and have a valid lifecycle.
		 * 
		 * @param [in] publisher Reference to the publisher to use
		 * @param [in] qos Quality of service to choose for MQTT message
		 * @param [in] retain Flag whether to retain the message on the server.
		 * @return Boolean flag whether the message was accepted in the message queue. 
		 * 
		 * @todo Currently the implementation of this function is redundant in Variable and Parameter.
		 * It is deliberately not moved to Figure as different implementations may occur in the future,
		 * but would be a valid option.
		*/
		bool mqtt(std::shared_ptr<MQTT::Publisher> publisher, int qos = 0, bool retain = false);
	};
	


}



template<typename T, int x, int y>
SOIL::Parameter<T, x, y>::Parameter(std::shared_ptr<SOIL::Element> parent, std::string uuid, std::string name, std::string description, std::string unit, bool constant, std::string ontology, Range<T> range, SOIL::TIME time) : SOIL::Figure<T, x, y>(parent, uuid, name, description, unit, ontology, range, time), constant(constant)
{
	if (uuid.substr(0, 3) != "PAR")
	{
		throw std::logic_error("UUIDs for parameters must start with PAR!");
	}

	HTTP::Resource::allowed_methods = { HTTP::Methods::GET, HTTP::Methods::OPTIONS, HTTP::Methods::HEAD, HTTP::Methods::PATCH };
}

template<typename T, int x, int y>
SOIL::Parameter<T,x,y>::~Parameter()
{
}

template<typename T, int x, int y>
inline std::shared_ptr<SOIL::Parameter<T, x, y> > SOIL::Parameter<T, x, y>::create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, bool constant, std::string ontology, Range<T> range, TIME time)
{
	Parameter<T, x, y>* parameter = new Parameter<T, x, y>(parent, uuid, name, description, unit, constant, ontology, range, time);
	return parameter->ptr();
}

template<typename T, int x, int y>
inline SOIL::Parameter<T, x, y>& SOIL::Parameter<T, x, y>::operator=(const Container<T, x, y>& value)
{
	Figure<T,x,y>::operator=(value);
	return *this;
}


template<typename T, int x, int y>
HTTP::Json SOIL::Parameter<T,x,y>::wjson(void)
{
	std::unique_lock<std::recursive_mutex> lock(Element::mutex);
	HTTP::Json json_root = Figure<T,x,y> ::wjson();
	json_root[_XPLATSTR("constant")] = HTTP::Json::boolean(constant);

	return json_root;
}

template<typename T, int x, int y>
void SOIL::Parameter<T,x,y>::read(void)
{
}

template<typename T, int x, int y>
inline void SOIL::Parameter<T, x, y>::write(void)
{
}


template<typename T, int x, int y>
inline HTTP::Response SOIL::Parameter<T, x, y>::handle_get(HTTP::Request message, std::smatch match)
{
	this->read();
	
	HTTP::Response response;
	HTTP::Json body = this->wjson();
	response.set_body(body);

	response.set_status_code(HTTP::Status::OK);

	return response;
}

template<typename T, int x, int y>
inline HTTP::Response SOIL::Parameter<T, x, y>::handle_patch(HTTP::Request message, std::smatch match)
{
	auto task  = message.extract_json();
	task.wait();
	HTTP::Json external_json = task.get();

	SOIL::TIME timestamp;

	if (external_json.has_field(_XPLATSTR("timestamp")))
	{
		timestamp = to_value<TIME>(external_json[_XPLATSTR("timestamp")]);
	}
	else
	{
		timestamp = SOIL::TIME::utc_now();
	}
	

	this->update(Container<T, x, y>(external_json[_XPLATSTR("value")]), timestamp);

	
	this->write();

	HTTP::Response response;
	response.set_body(this->wjson());
	response.set_status_code(HTTP::Status::Created);


	return response;
}

template<typename T, int x, int y>
inline bool SOIL::Parameter<T, x, y>::mqtt(std::shared_ptr<MQTT::Publisher> publisher, int qos, bool retain)
{
	std::string topic = boost::algorithm::join(this->fqid(), "/");
	return publisher->publish(topic, this->json(), qos, retain);
}



