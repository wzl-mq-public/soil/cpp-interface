#include "json_helpers.h"
#include "Types.h"

template <typename T>
web::json::value SOIL::to_json(const T& value)
{
	return web::json::value(value);
}

template<>
web::json::value SOIL::to_json<double>(const double& value)
{
	return web::json::value::number(value);
}

template<>
web::json::value SOIL::to_json<int64_t>(const int64_t& value)
{
	return web::json::value::number(value);
}

template<>
web::json::value SOIL::to_json<int>(const int& value)
{
	return web::json::value::number(value);
}

template<>
web::json::value SOIL::to_json<std::string>(const std::string& value)
{
	return web::json::value::string(utility::conversions::to_string_t(value));
}

template<>
web::json::value SOIL::to_json<SOIL::TIME>(const SOIL::TIME& value)
{
	return to_json<std::string>(value.rfc3339());
}

template<>
web::json::value SOIL::to_json<SOIL::ENUM>(const SOIL::ENUM& value)
{
	return to_json<std::string>(value.selected());
}

template<>
web::json::value SOIL::to_json<SOIL::BOOL>(const bool& value)
{
	return web::json::value::boolean(value);
}



template<>
double SOIL::to_value<double>(web::json::value value)
{
	return value.as_double();
}


template<>
int SOIL::to_value<int>(web::json::value value)
{
	return value.as_integer();
}

template<>
int64_t SOIL::to_value<int64_t>(web::json::value value)
{
	return static_cast<int64_t>(value.as_integer());
}

template<>
std::string SOIL::to_value<std::string>(web::json::value json)
{
	return utility::conversions::to_utf8string(json.as_string());
}

template<>
SOIL::TIME SOIL::to_value<SOIL::TIME>(web::json::value json)
{
	return SOIL::TIME(to_value<std::string>(json));
}

template<>
SOIL::ENUM SOIL::to_value<SOIL::ENUM>(web::json::value json)
{
	return SOIL::ENUM(to_value<std::string>(json));
}

template<>
bool SOIL::to_value<bool>(web::json::value json)
{
	return json.as_bool();
}