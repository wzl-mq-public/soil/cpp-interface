#include "Function.h"


SOIL::Function::Function(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology) : Element(parent, uuid, name, description, ontology)
{
	if (uuid.substr(0, 3) != "FUN")
	{
		throw std::logic_error("UUIDs for functions must start with FUN!");
	}

	allowed_methods = { HTTP::Methods::GET, HTTP::Methods::POST, HTTP::Methods::OPTIONS, HTTP::Methods::HEAD };
}

SOIL::Function::~Function()
{
}

std::shared_ptr<SOIL::Function> SOIL::Function::create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology)
{
	Function* function = new Function(parent, uuid, name, description, ontology);
	return function->ptr();
}

HTTP::Json SOIL::Function::wjson(void)
{
	HTTP::Json json_root = Element::wjson();
	json_root[_XPLATSTR("arguments")] = HTTP::Json::array();
	int i = 0;
	for (auto& a : arguments)
	{
		json_root[_XPLATSTR("arguments")][i] = a.second->wjson();
		i++;
	}

	json_root[_XPLATSTR("returns")] = HTTP::Json::array();
	i = 0;
	for (auto& r : returns)
	{
		json_root[_XPLATSTR("returns")][i] = r.second->wjson();
		i++;
	}
	json_root[_XPLATSTR("errors")] = HTTP::Json::array();
	return json_root;
}

HTTP::Response SOIL::Function::handle_get(HTTP::Request request, std::smatch match)
{
	HTTP::Response response;
	response.set_body(this->wjson());
	response.set_status_code(HTTP::Status::OK);
	return response;
}

HTTP::Response SOIL::Function::handle_post(HTTP::Request request, std::smatch match)
{
	auto task = request.extract_json();
	task.wait();

	std::map<std::string, HTTP::Json> arguments;
	HTTP::Json body = task.get();
	
	auto json_arguments = body[_XPLATSTR("arguments")].as_array();

	for (auto i = json_arguments.begin(); i != json_arguments.end(); i++)
	{
		auto json_argument = i->as_object();
		std::string uuid = SOIL::to_value<std::string>(json_argument[_XPLATSTR("uuid")]);
		HTTP::Json value = json_argument[_XPLATSTR("value")];

		arguments[uuid] = value;
	}

	std::map<std::string, HTTP::Json> returns = this->invoke(arguments);

	std::vector<web::json::value> json_returns;

	for (auto i = returns.begin(); i != returns.end(); i++)
	{
		web::json::value local;
		local[_XPLATSTR("uuid")] = SOIL::to_json<std::string>(i->first);
		local[_XPLATSTR("value")] = i ->second;
		json_returns.push_back(local);
	}

	HTTP::Json json;
	json[_XPLATSTR("returns")] =HTTP::Json::array(json_returns);

	HTTP::Response response;
	response.set_body(json);
	response.set_status_code(HTTP::Status::OK);

	return response;
}

std::map<std::string, HTTP::Json> SOIL::Function::invoke(std::map<std::string, HTTP::Json> arguments)
{
	throw std::logic_error("The method has not been implemented!");
}
