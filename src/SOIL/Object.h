#pragma once
#include "constants.h"
#include "Element.h"

namespace SOIL
{
	/**
	 * @brief Object Class
	 *
	 * Class implementing the SOIL Object type.
	 * The purpose of Objects is mainly to organize the overall system.
	 * Only Objects can have child items, which can be added or removed during runtime.
	 * Objects support HTTP GET (read), PUT (add child) and DELETE (remove child) verbs.
	 * This class directly inherits from Elememt.
	 * 
	 * @todo The HTTP handlers may be moved to protected if HTTP::Server is declared as friend class. Currently this is not done to alllow for greater flexibility. 
	*/
	class DLL Object :
		public Element
	{
	public:
		/**
		 * @brief Constructor
		 *
		 * Default constructor for SOIL elements, to be called from derived classes.
		 * @param [in] parent Shared pointer to parent object. Can be set to NULL for the creation of a root object
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 * 
		 * @pre The ontology referred to must be defined elsewhere and is decoupled from the code.
		*/
		Object(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology = "");

		/**
		 * @brief Destructor
		 * 
		 * Standard destructor. The main destruction occurs in the destructor of Element, i.e. the references to the child
		 * elements are deleted, e.g. they go out of scope is this was the last shared pointer.
		*/
		~Object();

		/**
		 * @brief Create new Object
		 *
		 * Create a new Object using the default constructor and return a shared pointer reference.
		 * This is the preferred method for manual creation with consistent lifecycle handling.
		 * @param [in] parent Shared pointer to parent object. Can be set to NULL for the creation of a root object
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 *
		 * @pre The ontology referred to must be defined elsewhere and is decoupled from the code.
		*/
		static std::shared_ptr<Object> create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology = "");

		/**
		 * @brief HTTP JSON
		 *
		 * Get a HTTP JSON object corresponding to the function including arguments and returns
		 * @return JSON object
		*/
		web::json::value wjson(void);

		/**
		 * @brief Read callback
		 *
		 * Read callback that can be implemented by deriving classes to perform custom build logic on read actions, e.g. update the value from an external storage.
		 * Declared virtual to make sure the derived method is called first. Any exception that may be occurring will be caught in the HTTP handler.
		*/
		virtual void read(void);

		/**
		 * @brief Insert callback
		 * 
		 * Function that is called when a new child object is insterted via HTTP PUT.
		 * This function muss be implemented by the deriving class.
		 * 
		 * @exception std::logic_error If the derived class does not contain an implementation, an exception is thrown here.
		 * The exception will be caught in the HTTP handler of the server and returns with an HTTP Internal Error (500) code.
		 * 
		 * @param [in] body HTTP JSON body passed to PUT
		 * @return HTTP JSON object corresponding to the created Object, as required per good HTTP practice.
		*/
		virtual HTTP::Json insert(HTTP::Json body);

		/**
		 * @brief Remove callback
		 * 
		 * Callback hook for subclasses to add custom business logic to the HTTP DELETE operation, e.g. to act on external resoruces.
		 * Otherwise the object will just be deleted.
		*/
		virtual void remove(void);

		/**
		 * @brief HTTP GET Handler
		 *
		 * Handler that is called by the server on HTTP requests on a GET method.
		 * This function returns a representation of the Object to the requesting party.
		 * It should not be overridden directly in subclasses, instead the `read()` function should be overriden.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_get(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief HTTP PUT Handler
		 *
		 * Handler that is called by the server on HTTP requests on a PUT method.
		 * This function inserts an element to the objects children.
		 * It should not be overridden directly in subclasses, instead the `HTTP::Json insert(HTTP::Json body)` function should be overriden.
		 * It is mandatory to implement a custom logic in the aforementioned function to allow PUT to properly work.
		 * If manual insertion is not foreseen, you may consider overriding `allowed_methods` without PUT.
		 * The minimum requirement to the body is that it contains an valid UUID key with value, the templates and further logic may be implemented by server.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_put(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief HTTP DELETE Handler
		 *
		 * Handler that is called by the server on HTTP requests on a DELETE method.
		 * This function deletes an element.
		 * It should not be overridden directly in subclasses, instead the `remove()` function should be overriden.
		 * It is mandatory to implement a custom logic in the aforementioned function to allow PUT to properly work.
		 * If manual deletaion is not foreseen, you may consider overriding `allowed_methods` without DELETE.
		 * 
		 * If no exception is thrown, an empty response with result code OK is returned, otherwise an Internal Error will be provided.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_delete(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief Get Pointer
		 *
		 * Return a shared pointer casted to the Object type to element itself.
		 * @return Casted pointer
		*/
		inline std::shared_ptr<Object> ptr(void)
		{
			return std::dynamic_pointer_cast<Object>(Element::self);
		}
	};
}

