#pragma once
#include "constants.h"
#include "Types.h"
#include "Element.h"
#include "Container.h"
#include "Range.h"
namespace SOIL
{
	/**
	 * @brief HTTP JSON datatype
	 * 
	 * Returns the datatype part of the SOIL-conformant JSON-representation of the figure, expressed as string.
	 * This function is declared in its specializations as it needs to be implemented expclicitiy for each datatype.
	 * 
	 * @tparam T Dataype under consideration
	 * @return HTTP JSON representaion as HTTP::Json::string
	*/
	template <typename T>
	HTTP::Json datatype(void);
	template<>
	DLL HTTP::Json datatype<double>(void);
	template<>
	DLL HTTP::Json datatype<bool>(void);
	template<>
	DLL HTTP::Json datatype<std::string>(void);
	template<>
	DLL HTTP::Json datatype<int64_t>(void);
	template<>
	DLL HTTP::Json datatype<int>(void);
	template<>
	DLL HTTP::Json datatype<SOIL::TIME>(void);
	template<>
	DLL HTTP::Json datatype<SOIL::ENUM>(void);


	/**
	 * @brief Intermediate class for Variable and Parameter that derives from Element
	 * 
	 * Intermediate class for Variable and Parameter that derives from Element as both share many properties.
	 * This class should not be instantianted directly and is abstract.
	 * The underlying data management completely relies on the templated Container class, henace many templates are passed on.
	 * 
	 * @tparam T Type of the data.
	 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
	 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
	*/
	template <typename T, int x=-1, int y=-1>
	class Figure : public Element
	{
	protected:
		/**
		 * @brief Data Timestamp
		 * 
		 * Timestamp of the data, i.e. the time which should be considered as physically related to the value.
		*/
		TIME time;

		/**
		 * @brief Unit
		 * 
		 * Unit of the stored value, expressed as UNECE code (e.g. MTR).
		*/
		std::string unit;

		/**
		 * @brief Value
		 * 
		 * Actual value that is currently held by the figure, which is represented as conatiner.
		*/
		Container<T, x, y>  value;

		/**
		 * @brief Range
		 * 
		 * Allowed range for the figure, expressed using the therefore designed Range class.
		*/
		Range<T> range;

		/**
		 * @brief Read callback
		 * 
		 * Read callback that can be implemented by deriving classes to perform custom build logic on read actions, e.g. update the value from an external storage.
		 * Declared virtual to make sure the derived method is called first.
		*/
		virtual void read(void) = 0;

		/**
		 * @brief Write callback
		 *
		 * Write callback that can be implemented by deriving classes to perform custom build logic on write actions, e.g.set a value to an external system.
		 * Declared virtual to make sure the derived method is called first.
		*/
		virtual void write(void) = 0;

	public:
		/**
		 * @brief Constructor
		 * 
		 * Standard constructor intialiazing the values which should be called from the constructor of deriving classes.
		 * @param [in] parent Shared pointer to parent object.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 * @param range [in] Allowed range for the variable values, defaults to an empty object, i.e. allowing all values
		 * @param time [in] Timestamp for the initial value, defaults to unset
		*/
		Figure(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, std::string ontology = "",  Range<T> range = Range<T>(), TIME time = TIME());

		/**
		 * @brief Destructor
		 * 
		 * Default destructor, without custom effort.
		*/
		~Figure();

		/**
		 * @brief Assignment operator
		 * 
		 * Assigns the value provided as container on the right hand side to the figure.
		 * 
		 * @exception std::range_error Throws an exception if the value to assign is outside the allowed range.
		 * 
		 * @param [in] value Value to assign
		 * @return Reference to the current Figure
		*/
		Figure<T, x, y>& operator =(const Container<T, x, y>& value);


		/**
		 * @brief Access Operator
		 * 
		 * Access Operator returning the container of the value. 
		 * @return Value container.
		*/
		Container<T, x, y>& operator* (void);

		/**
		 * @brief Check range
		 * 
		 * Check if value expressed as container object matches the range specified for this Figure.
		 * This may be useful prior to assignment to avoid exceptions.
		 * @param [in] value Value to check
		 * @return True if the value(s) is (are) in range, false else.
		*/
		bool check_range(const Container<T, x, y>& value) const;

		/**
		 * @brief Set Range
		 * 
		 * Set the range property of this figure.
		 * @param [in] range Range to set 
		*/
		void set_range(Range<T> range);

		/**
		 * @brief Set Time
		 *
		 * Set the time property of this figure.
		 * @param [in] time Time to set
		*/
		void set_time(TIME time);

		/**
		 * @brief Set Value
		 * 
		 * Assigns the value provided as container. This is currently equivalent to the assignment operator.
		 *
		 * @exception std::range_error Throws an exception if the value to assign is outside the allowed range.
		 *
		 * @param [in] value Value to assign
		 * 
		*/
		void set_value(const Container<T, x, y>& value);


		/**
		 * @brief HTTP JSON
		 *
		 * Get a HTTP JSON object corresponding to the current state of the Figure.
		 * This function provides a partial representation of the SOIL-conformant JSON representation of Variable and Parameter and
		 * may be called from their @c wjson() methods.
		 * @return JSON object
		*/
		HTTP::Json wjson(void) override;

		/**
		 * @brief Cast to container
		 * 
		 * Takes a value and returns the corresponding container
		 * @param [in] value Value to containerize 
		 * @return Container version
		 * 
		 * @todo This function seems error prone in the multidimensional case.
		*/

		Container<T, x, y> cast(T value);

		/**
		 * @brief Update
		 * 
		 * Update the figure setting a new value and timestamp.
		 * This is common scenario when dealing with measurement data.
		 * 
		 * @param [in] value Value to set
		 * @param [in] time Timestamp to assign
		*/
		virtual void update(const Container<T, x, y>& value, TIME time);
	};


}



template<typename T, int x, int y>
SOIL::Figure<T, x, y>::Figure(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, std::string ontology, Range<T> range, SOIL::TIME time) : SOIL::Element(parent, uuid, name, description, ontology), unit(unit), range(range), time(time)
{
}

template<typename T, int x, int y>
SOIL::Figure<T,x,y>::~Figure()
{
}


template<typename T, int x, int y>
HTTP::Json SOIL::Figure<T,x,y>::wjson(void)
{

	std::unique_lock<std::recursive_mutex> lock(mutex);
	HTTP::Json json_root = SOIL::Element::wjson();
	json_root[_XPLATSTR("timestamp"] = (time.is_null() || time.rfc3339() == "")) ? HTTP::Json::null() : HTTP::Json::string(utility::conversions::to_string_t(time.rfc3339()));
	json_root[_XPLATSTR("range")] = range.wjson();
	json_root[_XPLATSTR("unit")] = (unit.length() == 0) ? HTTP::Json::null() : SOIL::to_json(unit);
	
	HTTP::Json value_json = value.wjson();
	json_root[_XPLATSTR("value")] = value_json[_XPLATSTR("value")];
	json_root[_XPLATSTR("dimension")] = value_json[_XPLATSTR("dimension")];
	json_root[_XPLATSTR("datatype")] = datatype<T>();


	return json_root;
}

template<typename T, int x, int y>
SOIL::Container<T, x, y> SOIL::Figure<T, x, y>::cast(T value)
{
	return Container<T, x, y>(value);
}

template<typename T, int x, int y>
void SOIL::Figure<T,x,y>::read(void)
{
}

template<typename T, int x, int y>
SOIL::Figure<T, x, y>& SOIL::Figure<T,x,y>::operator=(const Container<T, x, y>& value)
{
	if (!check_range(value))
	{
		throw std::range_error("Value is out of range specified for Figure " + uuid);
	}
	this->value = value;
	return *this;
}


template<typename T, int x, int y>
SOIL::Container<T, x, y>& SOIL::Figure<T,x,y>::operator*(void)
{
	this->read();
	return value;
}

template<typename T, int x, int y>
bool SOIL::Figure<T, x, y>::check_range(const Container<T, x, y>& value) const
{
	return value.check_range(range);
}

template<typename T, int x, int y>
void SOIL::Figure<T,x,y>::set_range(Range<T> range)
{
	this->range = range;
}


template<typename T, int x, int y>
void SOIL::Figure<T,x,y>::set_time(TIME time)
{
	this->time = time;
}


template<typename T, int x, int y>
void SOIL::Figure<T, x, y>::set_value(const Container<T, x, y>& value)
{
	if (!check_range(value))
	{
		throw std::range_error("Value is out of range specified for Figure " + uuid);
	}
	*this = value;
}

template<typename T, int x, int y>
void SOIL::Figure<T,x,y>::update(const Container<T,x,y>& value, TIME time)
{
	std::unique_lock<std::recursive_mutex> lock(mutex);
	if (!check_range(value))
	{
		throw std::range_error("Value is out of range specified for Figure " + uuid);
	}
	*this = value;
	this->time = time;
}





