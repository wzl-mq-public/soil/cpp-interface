/**
 * @brief Default definitions for project
 *
 * File being referenced by many others containing cross-project defaults.
*/
#pragma once
#ifdef _WIN32
#        pragma warning(disable: 4251)
#		 pragma warning(disable: 4275)
#    ifdef _WINDLL
#        define DLL __declspec(dllexport)
#    else
#        define DLL __declspec(dllimport)
#    endif
#elif defined(__unix__)
#    define DLL __attribute__((visibility("default")))
#endif

#define _WIN32_WINNT 0x600
#ifdef _WIN32
#include <SDKDDKVer.h>
#endif