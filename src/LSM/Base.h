#pragma once
#include "SOIL/Variable.h"
#include "SOIL/Function.h"
#include "SOIL/Object.h"
#include "SOIL/Parameter.h"
#include "Device.h"
#include "Data.h"

namespace LSM
{
	namespace Base
	{
		class Name : public SOIL::Parameter<SOIL::STRING>
		{
		public:
			Name(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(parent, "PAR-Name", "Name", "Human readable name of the base.", "", true), device(device) {};
			~Name() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{
			}
		};

		class Position : public SOIL::Variable<SOIL::DOUBLE, 3>
		{
		public:
			Position(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Position",
				"Position",
				"Position in Cartesian Coordinates in meters",
				"MTR"), device(device) {};
			~Position() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};

		class Orientation : public SOIL::Variable<SOIL::DOUBLE, 4>
		{
		public:
			Orientation(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Orientation",
				"Orientation",
				"Orientation of the base as quaternion, measuered by gravitational gauge.",
				""), device(device) {};
			~Orientation() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};



		class Reset : public SOIL::Function
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Reset(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Function(parent, "FUN-Reset", "Reset", "Resets the base."), device(device)
			{
			}
			~Reset() {}

			std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments)
			{
				std::map<std::string, HTTP::Json> returns;
				return returns;
			}

		};

		class Weather : public SOIL::Parameter<SOIL::BOOL>
		{
		public:
			Weather(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-Weather",
				"Environment Sensor",
				"Flag whether the environment sensor is attached.",
				""), device(device) {};
			~Weather() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};

		class Temperature : public SOIL::Variable<SOIL::DOUBLE>
		{
		public:
			Temperature(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Temperature",
				"Temperature [deg C]",
				"Most recently measured temperature in degree celsius.",
				"DEG"
			), device(device) {};
			~Temperature() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};

		class Humidity : public SOIL::Variable<SOIL::DOUBLE>
		{
		public:
			Humidity(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Humidity",
				"Humidity [%]",
				"Most recently measured relative humidity in percent.",
				"",
				"",
				{ 0.0, 100.0 }
			), device(device) {};
			~Humidity() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};

		class Pressure : public SOIL::Variable<SOIL::DOUBLE>
		{
		public:
			Pressure(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Pressure",
				"Pressure [hPa]",
				"Most recently measured pressure in hPa.",
				"PAL"), device(device) {};
			~Pressure() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};

		class Calibration : public SOIL::Parameter<SOIL::STRING>
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Calibration(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-Calibration",
				"Calibration Identifier",
				"String unambigiously identifying a calibration certificate",
				"",
				true
			), device(device) {}
			~Calibration() {}

		};

		class State : public SOIL::Parameter<SOIL::ENUM>
		{
		public:
			State(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-State",
				"State",
				"General state of the entity",
				"",
				false,
				"",
				SOIL::Range<SOIL::ENUM>({ "OK", "WARNING", "ERROR", "MAINTENANCE" })
			), device(device) {
				value = SOIL::ENUM("OK", { "OK", "WARNING", "ERROR", "MAINTENANCE" });
			}
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void) {

			}
		};

		class Model : public SOIL::Object
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			std::shared_ptr<Position> position;
			std::shared_ptr<Orientation> orientation;
			std::shared_ptr<Weather> weather;
			std::shared_ptr<Temperature> temperature;
			std::shared_ptr<Humidity> humidity;
			std::shared_ptr<Pressure> pressure;
			std::shared_ptr<Calibration> calibration;

		public:
			Model(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Object(parent, "OBJ-Base", "Base", "Laser Tracker Base Object"), device(device)
			{
				new Name(self, device);
				position = std::dynamic_pointer_cast<Position>((new Position(self, device))->ptr());
				orientation = std::dynamic_pointer_cast<Orientation>((new Orientation(self, device))->ptr());
				new Reset(self, device);
				calibration = std::dynamic_pointer_cast<Calibration>((new Calibration(self, device))->ptr());

				temperature = std::dynamic_pointer_cast<Temperature>((new Temperature(self, device))->ptr());
				humidity = std::dynamic_pointer_cast<Humidity>((new Humidity(self, device))->ptr());
				pressure = std::dynamic_pointer_cast<Pressure>((new Pressure(self, device))->ptr());
				new State(self, device);

			}
			~Model() {}

			void update(VIRTUAL::Data& data, std::string nonce, boost::posix_time::ptime time);
		};
	}
}