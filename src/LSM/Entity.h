#pragma once
#include "SOIL/Variable.h"
#include "SOIL/Function.h"
#include "SOIL/Object.h"
#include "SOIL/Parameter.h"
#include "MQTT/Publisher.h"
#include "SIGN/Signer.h"
#include "Device.h"
#include "Data.h"

#include <regex>

namespace LSM
{

	namespace Entity
	{
		class Name : public SOIL::Parameter<SOIL::STRING>
		{
		public:
			Name(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-Name",
				"Name",
				"Human readable name of the target.",
				""), device(device) {};
			~Name() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{
			}
		};

		class Type : public SOIL::Parameter<SOIL::STRING>
		{
		public:
			Type(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-Type",
				"Type",
				"Target Type",
				""), device(device) {};
			~Type() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{

			}
		};

		class State : public SOIL::Parameter<SOIL::ENUM>
		{
		public:
			State(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-State",
				"State",
				"General state of the entity",
				"",
				false,
				"",
				SOIL::Range<SOIL::ENUM>({ "OK", "WARNING", "ERROR", "MAINTENANCE" })
			), device(device) {
				value = SOIL::ENUM("OK", { "OK", "WARNING", "ERROR", "MAINTENANCE" });
			}
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{
			};
		};

		class Mode : public SOIL::Parameter<SOIL::ENUM>
		{
		public:
			Mode(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-Mode",
				"Mode",
				"Acquisition Mode of the entity",
				"",
				false,
				"",
				SOIL::Range<SOIL::ENUM>({ "CONTINUOUS", "TRIGGERED", "EXTERNAL", "IDLE" })
			), device(device) {
				value = SOIL::ENUM("IDLE", { "CONTINUOUS", "TRIGGERED", "EXTERNAL", "IDLE" });
			}
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{

			};

			void write(void)
			{

			}
		};

		class Position : public SOIL::Variable<SOIL::DOUBLE, 3>
		{
		public:
			Position(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Position",
				"Position [m]",
				"Position in Cartesian Coordinates in meter.",
				"MTR"
			), device(device) {};
			~Position() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		};

		class Orientation : public SOIL::Variable<SOIL::DOUBLE, 4>
		{
		public:
			Orientation(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Variable(
				parent,
				"VAR-Orientation",
				"Orientation",
				"Orientation of the target as quaternion. Only applies to smart targets.",
				""), device(device) {};
			~Orientation() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{
				value.set_null(true);
			}
		};

		class Reset : public SOIL::Function
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Reset(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Function(parent, "FUN-Reset", "Reset", "Resets the entity."), device(device)
			{
			}
			~Reset() {}

			std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments)
			{
				std::map<std::string, HTTP::Json> returns;
				return returns;
			}

		};

		class Trigger : public SOIL::Function
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Trigger(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Function(parent, "FUN-Trigger", "Trigger", "Triggers a measurement."), device(device)
			{
				add_argument<SOIL::STRING>("PAR-Nonce", "Nonce", "Nonce the measurements shall be marked with", "");
				add_argument<SOIL::INT>("PAR-Count", "Count", "Number of measurements to trigger", "", SOIL::Range<SOIL::INT>(1, 1000), SOIL::Container<SOIL::INT>(1));
			}
			~Trigger() {}

			std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments)
			{
				std::map<std::string, HTTP::Json >returns;
				SOIL::Container<SOIL::STRING> nonce(arguments["PAR-Nonce"]);
				SOIL::Container<SOIL::INT> count(arguments["PAR-Count"]);

				return returns;
			}

		};


		class Calibration : public SOIL::Parameter<SOIL::STRING>
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Calibration(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(parent, "PAR-Calibration", "Calibration", "Calibration Identifier", ""), device(device) {}
			~Calibration() {}

		};

		class Model : public SOIL::Object
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			std::shared_ptr<Position> position;
		public:
			Model(std::shared_ptr<SOIL::Element> parent, std::string name, std::shared_ptr<VIRTUAL::Device> device) : Object(parent, "OBJ-" + std::regex_replace(name, std::regex(" "), "_"), name, "Entity Object"), device(device)
			{
				new Name(self, device);
				new Type(self, device);
				position = std::dynamic_pointer_cast<Entity::Position>((new Position(self, device))->ptr());
				new Orientation(self, device);
				new Reset(self, device);
				new State(self, device);
				new Trigger(self, device);
				new Mode(self, device);
				new Calibration(self, device);

			}
			~Model() {}

			void update(VIRTUAL::Data& data, std::string nonce, boost::posix_time::ptime time);
			void remove(void) override;

		};
	}
}