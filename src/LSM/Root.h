#pragma once
#include "SOIL/Variable.h"
#include "SOIL/Parameter.h"
#include "SOIL/Function.h"
#include "SOIL/Object.h"
#include "SOIL/Container.h"
#include "SOIL/Range.h"
#include "Model.h"
#include "Device.h"
#include "SOIL/Time.h"


namespace LSM
{
	namespace Root
	{

		class Reset : public SOIL::Function
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Reset(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Function(parent, "FUN-Reset", "Reset", "Reinitializes the device."), device(device)
			{
			}
			~Reset() {}

			std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments) override
			{
				std::map<std::string, HTTP::Json> returns;
				return returns;
			}

		};

		class Shutdown : public SOIL::Function
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Shutdown(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Function(parent, "FUN-Shutdown", "Shutdown", "Shuts down the device."), device(device)
			{
			}
			~Shutdown() {}

			std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments)
			{
				std::map<std::string, HTTP::Json> returns;
				return returns;
			}

		};

		class State : public SOIL::Parameter<SOIL::ENUM>
		{
		public:
			State(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-State",
				"State",
				"State of the device",
				"",
				false,
				"",
				SOIL::Range<SOIL::ENUM>({ "UNINITIALIZED","ERROR","WARNING","OPERATING","MAINTENANCE" })
			), device(device) {
				value = SOIL::ENUM("UNINITIALIZED", { "UNINITIALIZED","ERROR","WARNING","OPERATING","MAINTENANCE" });
			};
			~State() {};
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			void read(void)
			{
				set_time(SOIL::TIME::utc_now());
			}
		};

		class APIVersion : public SOIL::Parameter<SOIL::INT>
		{
		public:
			APIVersion(std::shared_ptr<SOIL::Element> parent) : Parameter(
				parent,
				"PAR-APIVersion",
				"API Version",
				"Interface version as incrementing integer",
				"",
				true
			) {
				update(1, SOIL::TIME::utc_now());
			};
			~APIVersion() {};
		private:
			void read(void)
			{

			}
		};

		class Manufacturer : public SOIL::Parameter<SOIL::STRING>
		{
		public:
			Manufacturer(std::shared_ptr<SOIL::Element> parent) : Parameter(
				parent,
				"PAR-Manufacturer",
				"Manufacturer",
				"Name of the manufacturer of the device.",
				"",
				true
			) {
				set_value(std::string("WZL | RWTH Aachen University"));
			};
			~Manufacturer() {};
		};

		class SystemTime : public SOIL::Parameter<SOIL::TIME>
		{
		public:
			SystemTime(std::shared_ptr<SOIL::Element> parent) : Parameter(
				parent,
				"PAR-Time",
				"Time",
				"Currrent UTC system time.",
				""
			) {};
			~SystemTime() {};
		private:
			void read(void)
			{
				set_value(SOIL::Container<SOIL::TIME>(SOIL::TIME::utc_now()));
			}
		};


		class Model : public SOIL::Object
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			std::shared_ptr<LSM::Model> lsm;
		public:
			Model(std::shared_ptr<VIRTUAL::Device> device) : Object(NULL, "OBJ-VIRTUAL", "WZL Virtual", "WZL Virtual LSM Device"), device(device) {
				new Reset(self, device);
				new Shutdown(self, device);
				new State(self, device);
				new APIVersion(self);
				new Manufacturer(self);
				new SystemTime(self);
				lsm = std::dynamic_pointer_cast<LSM::Model>((new LSM::Model(self, device))->ptr());
			};

			~Model() {};

			void update_lsm()
			{
				lsm->update_model();
			}
		};
	}
}