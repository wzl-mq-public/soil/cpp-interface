#include "MQTT/Publisher.h"
#include "UDP/Broadcast.h"
#include "REST/Server.h"
#include "SOIL/Object.h"
#include "SOIL/Variable.h"
#include "SOIL/Types.h"
#include "SIGN/Hasher.h"
#include "SIGN/Signer.h"
#include "Device.h"
#include "Root.h"
#include <thread>
#include <iostream>
#include <chrono>




int main(int argc, char** argv)
{
	try
	{
		std::cout << "############ DEVICE ###########" << std::endl;
		std::shared_ptr<VIRTUAL::Device> device(new VIRTUAL::Device());

		std::cout << "############ SOIL #############" << std::endl;
		std::shared_ptr<LSM::Root::Model> root_object(new LSM::Root::Model(device));


		std::cout << "############ MQTT ############" << std::endl;
		MQTT::Configuration mqtt_config("../../../../assets/mqtt.json");
		mqtt_config.certificate_authority = "../../../../assets/MQTT-CA.pem";

		std::shared_ptr<MQTT::Publisher> mqtt_publisher = std::make_shared<MQTT::Publisher>("cpp-test");
		mqtt_publisher->configure(mqtt_config);
		std::cout << "[INFO] MQTT Connecting to " << mqtt_config.uri() << std::endl;
		mqtt_publisher->connect();
		std::cout << "[OK] MQTT Connected" << std::endl;


		std::cout << "############ REST ############" << std::endl;
		HTTP::Server http_server("http://localhost:8000");
		http_server.add("/?(.*)", root_object);
		http_server.open();
		std::cout << "[OK] HTTP Server listening on http://localhost:8000. Press Enter to quit..." << std::endl;
		std::string s;
		std::getline(std::cin, s);
		http_server.close();
		std::cout << "[OK] Closing HTTP Server" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
		mqtt_publisher->disconnect();
		std::cout << "[OK] MQTT Disconnect" << std::endl << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));

		root_object.reset();
		device.reset();

		return 0;
	}
	catch (std::exception& e)
	{
		std::cout << "[ERROR] " << e.what() << std::endl;
		
		return 1;
	}

}