#pragma once
#include "Data.h"
#include <vector>
#include <string>

namespace VIRTUAL
{
	class Device
	{
	private: 
		std::vector<std::string> targets;
	public:
		Device();
		std::vector<std::string> list_targets(void);
	};
}

