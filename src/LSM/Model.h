#pragma once
#include "SOIL/Variable.h"
#include "SOIL/Function.h"
#include "SOIL/Object.h"
#include "SOIL/Parameter.h"
#include "Device.h"
#include "Base.h"
#include "Entity.h"

namespace LSM
{
	namespace LSM
	{

		class Calibration : public SOIL::Parameter<SOIL::STRING>
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Calibration(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Parameter(
				parent,
				"PAR-Calibration",
				"Calibration Identifier",
				"String unambigiously identifying a calibration certificate",
				"",
				true
			), device(device) {}
			~Calibration() {}

		};

		class Covariance : public SOIL::Function
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Covariance(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Function(parent, "FUN-Covariance", "Covariance", "Obtain the covariance for two arbitrary variables."), device(device)
			{
				add_argument<SOIL::STRING, 0>("PAR-UUIDS", "UUIDs", "List of UUIDs for which to return covariance.", "");
			}
			~Covariance() {}

			std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments)
			{
				std::map<std::string, HTTP::Json> returns = Function::invoke(arguments);
				throw std::runtime_error("Cross-Covariance is not implemented for laser tracker!");
				return returns;
			}


		};

		class Bases : public SOIL::Object
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			std::shared_ptr<Base::Model> base;
		public:
			Bases(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Object(parent, "OBJ-Bases", "Bases", "List of Bases (1 in case of a Laser Tracker)"), device(device)
			{
				base = std::dynamic_pointer_cast<Base::Model> ((new Base::Model(self, device))->ptr());
			}
			~Bases() {}

			void update_bases()
			{
				//device->get_base()->set_soil(base);
			}

		};


		class Entities : public SOIL::Object
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
		public:
			Entities(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Object(parent, "OBJ-Entities", "Entities", "List of available entities."), device(device)
			{
				update_entities();
			}
			~Entities() {}

			void read(void)
			{

			}

			HTTP::Json insert(HTTP::Json body) override
			{
				std::string uuid = SOIL::to_value<std::string>(body[_XPLATSTR("uuid")]);
				if (uuid.substr(0, 4) != "OBJ-")
				{
					throw std::runtime_error("Invalid UUID, must start with OBJ-!");
				}
				std::string name = uuid.substr(4, uuid.length() - 4);
				return children[uuid]->wjson();
			}

			void update_entities(void)
			{
				std::vector<std::string> targets = device->list_targets();
				std::vector<std::string> stale_targets;

				for (auto& c : children)
				{
					bool found = false;
					for (auto& t : targets)
					{
						if (c.first.substr(4) == std::regex_replace(t, std::regex(" "), "_"))
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						stale_targets.push_back(c.first);
					}
				}

				for (auto& t : targets)
				{
					if (children.count("OBJ-" + std::regex_replace(t, std::regex(" "), "_")) == 0)
					{
						new Entity::Model(self, t, device);
					}
				}

				for (auto& s : stale_targets)
				{
					children.erase(s);
				}
			}

		};

		class Model : public SOIL::Object
		{
		private:
			std::shared_ptr<VIRTUAL::Device> device;
			std::shared_ptr<Entities> entities;
			std::shared_ptr<Bases> bases;
		public:
			Model(std::shared_ptr<SOIL::Element> parent, std::shared_ptr<VIRTUAL::Device> device) : Object(parent, "OBJ-LSM", "LSM Subsystem", "Large Scale Metrology Subsystem with defined functions."), device(device)
			{
				//new Reset(self, device);
				new Calibration(self, device);
				bases = std::dynamic_pointer_cast<Bases>((new Bases(self, device))->ptr());
				entities = std::dynamic_pointer_cast<Entities>((new Entities(self, device))->ptr());
				new Covariance(self, device);
			}
			~Model() {}

			void update_model(void)
			{
				entities->update_entities();
				bases->update_bases();
			}

			void update_entities(void)
			{
				entities->update_entities();
			}

			void update_bases(void)
			{
				bases->update_bases();
			}

			void update(VIRTUAL::Data& data, std::string nonce, boost::posix_time::ptime time);

		};
	}
}
