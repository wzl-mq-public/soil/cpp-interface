#include "Device.h"

VIRTUAL::Device::Device()
{
    targets = { "A", "B", "C", "D" };
}

std::vector<std::string> VIRTUAL::Device::list_targets(void)
{
    return targets;
}
