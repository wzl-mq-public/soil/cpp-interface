#include "MQTT/Publisher.h"
#include "UDP/Broadcast.h"
#include "REST/Server.h"
#include "SOIL/Object.h"
#include "SOIL/Variable.h"
#include "SOIL/Types.h"
#include "SIGN/Hasher.h"
#include "SIGN/Signer.h"
#include <thread>
#include <iostream>
#include <chrono>



int main(int argc, char** argv)
{
	try
	{
		std::cout << "############ SOIL #############" << std::endl;
		std::shared_ptr<SIGN::Signer> signer(new SIGN::Signer("../../../../assets/private-device-key.pem"));
		std::cout << "[INFO] " << signer->openssl_version() << std::endl;
		std::shared_ptr<SOIL::Object> root_object = SOIL::Object::create(NULL, "OBJ-ROOT", "Root Object", "Root Object and entry point for the model.");
		std::shared_ptr<SOIL::Object> lsm_object = SOIL::Object::create(root_object, "OBJ-LSM", "LSM System", "LSM system root object");
		std::shared_ptr<SOIL::Object> entities_object = SOIL::Object::create(lsm_object, "OBJ-Entities", "Mobile Entities", "Mobile entities of the LSM system");
		std::shared_ptr<SOIL::Object> target_A123 = SOIL::Object::create(entities_object, "OBJ-A123", "Target A123", "Target A123 of the LSM system");
		std::shared_ptr<SOIL::Variable<double, 3> > variable = SOIL::Variable<double, 3>::create(target_A123, "VAR-Position", "Position", "A dummy position variable for testing", "MTR", "");
		// NOTE: In above calls, the ptr->() function is used to avoid double allocation of shared pointer and double life cycle accounting

		variable->update(std::vector<double>({ 1.0, 1.5, 2.0 }), SOIL::TIME::utc_now(), "LaVA Test");
		variable->set_covariance(std::vector<std::vector<double> >({ { 0.01, 0, 0 },{0, 0.01, 0},{ 0, 0, 0.01 } }));
		variable->sign(signer);


		std::cout << "[INFO] " << "--- Exemplary JSON Serialization ---" << std::endl;
		std::cout << variable->json() << std::endl << std::endl;
		std::cout << "[INFO] " << "------------------------------------" << std::endl;


		std::cout << "############ MQTT ############" << std::endl;
		MQTT::Configuration mqtt_config("../../../../assets/mqtt.json");
		mqtt_config.certificate_authority = "../../../../assets/MQTT-CA.pem";

		std::shared_ptr<MQTT::Publisher> mqtt_publisher = std::make_shared<MQTT::Publisher>("cpp-test");
		mqtt_publisher->configure(mqtt_config);
		std::cout << "[INFO] MQTT Connecting to " << mqtt_config.uri() << std::endl;
		mqtt_publisher->connect();
		std::cout << "[OK] MQTT Connected" << std::endl;

		std::cout << "[INFO] Running Loop Test" << std::endl;

		auto begin = std::chrono::steady_clock::now();
		for (int i = 0; i < 1000; i++)
		{
			variable->update(std::vector<double>({ 1.0 * i, 2.0 * i, 3.0 * i }), SOIL::TIME::utc_now(), "Loop Test Update");
			variable->set_covariance(std::vector<std::vector<double> >({ { 1.0 * i,0,0 },{ 0,1.0 * i,0 },{ 0,0,1.0 * i } }));
			variable->sign(signer);
			variable->json();
			variable->fqid();
			variable->mqtt(mqtt_publisher);
		}
		auto end = std::chrono::steady_clock::now();
		std::cout << "[INFO] " << "Average update, serialization and publish cycle: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.0 << " ms" << std::endl << std::endl;

		std::this_thread::sleep_for(std::chrono::milliseconds(500));
		mqtt_publisher->disconnect();
		std::cout << "[OK] MQTT Disconnect" << std::endl << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));


		std::cout << "############ UDP #############" << std::endl;
		UDP::Configuration udp_config("../../../../assets/udp.json");
		UDP::Broadcast udp_broadcast(1);
		udp_broadcast.configure(udp_config);
		udp_broadcast.send(variable->json());
		std::cout << "[OK] UDP Broadcast sent to localhost on port " << udp_config.clients["127.0.0.1"] << std::endl << std::endl;


		std::cout << "############ REST ############" << std::endl;
		HTTP::Server http_server("http://localhost:8000");
		http_server.add("/?(.*)", root_object);
		http_server.open();
		std::cout << "[OK] HTTP Server listening on http://localhost:8000. Press Enter to quit..." << std::endl;
		std::string s;
		std::getline(std::cin, s);
		http_server.close();
		std::cout << "[OK] Closing HTTP Server" << std::endl;

		root_object.reset();

		return 0;
	}
	catch (std::exception& e)
	{
		std::cout << "[ERROR] " << e.what() << std::endl;
		
		return 1;
	}

}