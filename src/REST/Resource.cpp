#include "Resource.h"

web::json::value HTTP::Resource::request_info(Request message, std::smatch match)
{
	web::json::value headers;
	for (web::http::http_headers::iterator i = message.headers().begin(); i != message.headers().end(); i++)
	{
		headers[i->first] = web::json::value::string(i->second);
	}
	web::json::value body;
	body[_XPLATSTR("method")] = web::json::value::string(message.method());
	body[_XPLATSTR("headers")] = headers;
	auto task = message.extract_string();
	task.wait();
	body[_XPLATSTR("body")] = web::json::value::string(task.get());
	web::uri uri = message.absolute_uri();
	web::json::value url;
	url[_XPLATSTR("path")] = web::json::value::string(uri.path());
	url[_XPLATSTR("scheme")] = web::json::value::string(uri.scheme());
	url[_XPLATSTR("query")] = web::json::value::string(uri.query());
	url[_XPLATSTR("host")] = web::json::value::string(uri.host());
	url[_XPLATSTR("port")] = web::json::value::number(uri.port());

	body[_XPLATSTR("url")] = url;
	body[_XPLATSTR("remote_address")] = web::json::value(message.remote_address());

	std::vector<web::json::value> matches;
	for (size_t i = 0; i < match.size(); i++)
	{
		std::string m = match.str(i);
		matches.push_back(web::json::value::string(utility::conversions::to_string_t(m)));
	}

	body[_XPLATSTR("matches")] = web::json::value::array(matches);

	return body;
}

HTTP::Response HTTP::Resource::fallback(Request message, std::smatch match)
{
	
	web::json::value body = request_info(message, match);
	Response response;
	response.set_body(body);
	response.set_status_code(Status::NotImplemented);
	//this->apply_headers(response);
	return response;
}

void HTTP::Resource::apply_headers(Response & response)
{
	utility::string_t allowed_methods;
	for (auto i = this->allowed_methods.begin(); i != this->allowed_methods.end(); i++)
	{
		if (i != this->allowed_methods.begin())
		{
			allowed_methods.append(_XPLATSTR(", "));
		}
		allowed_methods.append(*i);
	}
	response.headers().add(_XPLATSTR("Access-Control-Allow-Origin"), utility::conversions::to_string_t(allowed_origins));
	response.headers().add(_XPLATSTR("Access-Control-Allow-Headers"), _XPLATSTR("content-type"));
	response.headers().add(_XPLATSTR("Allow"), allowed_methods);
	response.headers().add(_XPLATSTR("Access-Control-Allow-Methods"), allowed_methods);
	response.headers().add(_XPLATSTR("Content-Type"), utility::conversions::to_string_t(content_type));
}

HTTP::Resource::Resource()
{
	allowed_origins = "*";
	allowed_methods = { web::http::methods::GET, web::http::methods::DEL, web::http::methods::PATCH, web::http::methods::POST, web::http::methods::OPTIONS, web::http::methods::HEAD, web::http::methods::PUT };
	content_type = "application/json";
}

HTTP::Resource::~Resource()
{
}

HTTP::Response HTTP::Resource::handle(Request message, std::smatch match)
{
	Response response;
	try
	{
		if (message.method() == Methods::GET)
		{
			response = this->handle_get(message, match);
		}
		else if (message.method() == Methods::POST)
		{
			response = this->handle_post(message, match);
		}
		else if (message.method() == Methods::PATCH)
		{
			response = this->handle_patch(message, match);
		}
		else if (message.method() == Methods::DEL)
		{
			response = this->handle_delete(message, match);
		}
		else if (message.method() == Methods::PUT)
		{
			response = this->handle_put(message, match);
		}
		else if (message.method() == Methods::OPTIONS)
		{
			response = this->handle_options(message, match);
		}
		else if (message.method() == Methods::HEAD)
		{
			response = this->handle_head(message, match);
		}
		this->apply_headers(response);
	}
	catch (std::exception& exception)
	{
		response = handle_exception(message, exception, match);

	}
	return response;
}

HTTP::Response HTTP::Resource::handle_get(Request message, std::smatch match)
{
	return this->fallback(message, match);
}

HTTP::Response HTTP::Resource::handle_post(Request message, std::smatch match)
{
	return this->fallback(message, match);
}

HTTP::Response HTTP::Resource::handle_patch(Request message, std::smatch match)
{
	return this->fallback(message, match);
}


HTTP::Response HTTP::Resource::handle_delete(Request message, std::smatch match)
{
	return this->fallback(message, match);
}


HTTP::Response HTTP::Resource::handle_put(Request message, std::smatch match)
{
	return this->fallback(message, match);
}


HTTP::Response HTTP::Resource::handle_options(Request message, std::smatch match)
{
	//return this->fallback(message, match);
	Response response;
	response.set_status_code(Status::OK);
	return response;
}

HTTP::Response HTTP::Resource::handle_head(Request message, std::smatch match)
{
	return this->fallback(message, match);
}

HTTP::Response HTTP::Resource::handle_exception(Request message, std::exception & exception, std::smatch match)
{
	web::json::value body = request_info(message, match);
	body[_XPLATSTR("error")] = web::json::value(utility::conversions::to_string_t(exception.what()));
	Response response;
	response.set_body(body);
	response.set_status_code(Status::InternalError);
	this->apply_headers(response);
	return response;
}
