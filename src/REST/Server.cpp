#include "Server.h"

using namespace std;
using namespace web;
using namespace utility;
using namespace http;
using namespace web::http::experimental::listener;

HTTP::Server::Server(std::string url) : listener(utility::conversions::to_string_t(url))
{
	listener.support(std::bind(&Server::handle, this, std::placeholders::_1));
	listener.support(web::http::methods::OPTIONS, std::bind(&Server::handle, this, std::placeholders::_1));
}


HTTP::Server::~Server()
{
}

void HTTP::Server::handle(http_request message)
{
	for (auto i = resources.begin(); i != resources.end(); i++)
	{
		std::smatch match;
		std::string line = utility::conversions::to_utf8string(message.relative_uri().to_string());
		std::regex regex(i->first);
		if (std::regex_search(line, match, regex))
		{
			Response response = i->second->handle(message, match);
			message.reply(response);
			return;
		}
	}
	Response response = default_resource.handle(message);
	message.reply(response);

	
}

void HTTP::Server::add(std::string path, std::shared_ptr<Resource> resource)
{
	resources.push_back(std::make_pair(path, resource));
}

void HTTP::Server::remove(std::string path)
{
	for (auto i = resources.begin(); i != resources.end(); i++)
	{
		if (i->first == path)
		{
			resources.erase(i);
			break;
		}
	}
}
