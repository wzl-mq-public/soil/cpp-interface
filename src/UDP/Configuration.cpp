#include "Configuration.h"
#include <nlohmann/json.hpp>
#include <fstream>

using json = nlohmann::json;

UDP::Configuration::Configuration()
{
	clients.clear();
}

UDP::Configuration::Configuration(std::string filename)
{
	std::ifstream infile(filename);
	json j;
	infile >> j;

	for (json::iterator it = j["clients"].begin(); it != j["clients"].end(); it++)
	{
		clients[(*it)["address"]] = (*it)["port"];
	}
}

UDP::Configuration::~Configuration()
{
	clients.clear();
}
