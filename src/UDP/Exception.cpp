#include "Exception.h"

UDP::Exception::Exception(const char* message) : std::runtime_error(message)
{
}

UDP::Exception::~Exception()
{
}