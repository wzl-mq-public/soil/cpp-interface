#include "Broadcast.h"
#include "Exception.h"


UDP::Broadcast::Broadcast(int n_threads)
{
	threads.reset(new boost::thread_group);
	work.reset(new boost::asio::io_service::work(io_service));
	for (int i = 0; i < n_threads; i++)
	{
		threads->create_thread(boost::bind(&boost::asio::io_service::run, &io_service));
	}
	socket.reset(new boost::asio::ip::udp::socket(io_service));//, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), 0)));
	socket->open(boost::asio::ip::udp::v4());
	boost::asio::socket_base::reuse_address reuse_address(true);
	socket->set_option(boost::asio::socket_base::reuse_address(true));
	//socket->set_option(boost::asio::socket_base::broadcast(true));

	endpoints.resize(0);
}


UDP::Broadcast::~Broadcast()
{
	if (socket.use_count() > 0)
	{
		if (socket->is_open())
		{
			socket->shutdown(boost::asio::ip::udp::socket::shutdown_both);
			socket->close();
		}
		socket.reset();
		endpoints.resize(0);
	}
	work.reset();
	threads->join_all();
	threads.reset();
}

void UDP::Broadcast::add_client(std::string ip, unsigned int port)
{
	std::unique_lock<std::mutex> lock(endpoints_mutex);
	try
	{
		std::vector<boost::asio::ip::udp::endpoint>::iterator it;
		boost::asio::ip::address address = boost::asio::ip::address::from_string(ip);
		for (it = endpoints.begin(); it != endpoints.end(); it++)
		{
			if ((*it).address() == address)
			{
				throw UDP::Exception("Client is already among broadcast recipients!");
			}
		}
		endpoints.push_back(boost::asio::ip::udp::endpoint(address, port));
	}
	catch (std::exception& exception)
	{
		std::string message = std::string("Error while adding UDP client: ") + exception.what();
		throw UDP::Exception(message.c_str());
	}
}

void UDP::Broadcast::remove_client(std::string ip)
{
	std::unique_lock<std::mutex> lock(endpoints_mutex);
	try
	{
		std::vector<boost::asio::ip::udp::endpoint>::iterator it;
		boost::asio::ip::address address = boost::asio::ip::address::from_string(ip);
		for (it = endpoints.begin(); it != endpoints.end(); it++)
		{
			if ((*it).address() == address)
			{
				break;
			}
		}
		if (it == endpoints.end())
		{
			throw UDP::Exception("Client is not in broadcast list!");
		}
		endpoints.erase(it);
	}
	catch (std::exception& exception)
	{
		std::string error_message = std::string("Error while removing UDP client: ") + exception.what();
		throw UDP::Exception(error_message.c_str());
	}
}

void UDP::Broadcast::_send(std::string message)
{
	if (socket.use_count() == 0)
	{
		return;
	}
	if (socket->is_open())
	{
		std::unique_lock<std::mutex> lock(endpoints_mutex);
		std::shared_ptr<std::string> local_message(new std::string(message));
		std::vector<boost::asio::ip::udp::endpoint>::iterator it;
		for (it = endpoints.begin(); it != endpoints.end(); it++)
		{
			socket->send_to(boost::asio::buffer(*local_message), *it);
			//socket->send_to(boost::asio::buffer(*local_message), *it, boost::bind(&UDP::Broadcast::handle, this, message, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		}
	}
}

void UDP::Broadcast::send(std::string message)
{
	io_service.post(boost::bind(&UDP::Broadcast::_send, this, message + "\r\n"));
}

void UDP::Broadcast::send(std::vector<std::string> messages)
{
	std::string message = "";
	for (std::vector<std::string>::iterator it = messages.begin(); it != messages.end(); it++)
	{
		message += (*it) + "\r\n";
	}
	io_service.post(boost::bind(&UDP::Broadcast::_send, this, message));
}

/*
void UDP::Broadcast::handle(std::shared_ptr<std::string> message, const boost::system::error_code & error, std::size_t bytes)
{
	std::string error_message = std::string("Error while sending UDP: ") + error.message();
	throw UDP::Exception(error_message.c_str());
}
*/

void UDP::Broadcast::configure(UDP::Configuration config)
{
	for (UDP::Configuration::iterator it = config.clients.begin(); it != config.clients.end(); it++)
	{
		this->add_client(it->first, it->second);
	}
}
