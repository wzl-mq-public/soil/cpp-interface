#pragma once
#include "constants.h"
#include <map>
#include <string>
namespace  UDP
{
	/**
	 * @brief UDP Broadcast Configuration
	 * 
	 * Class to simplify the configuration of UDP broadcasts, i.e. by providing a list of client endpoints that can be populated from a JSON file.
	 * For simplicity, all members are public.
	*/
	class DLL Configuration
	{
	public:
		/**
		 * @brief Iterator
		 * 
		 * Typedefinition to provide an iterator-sytled access
		*/
		typedef std::map<std::string, int>::iterator iterator;

		/**
		 * @brief List of clients
		 * 
		 * List of clients, implemented as map where the IPv4 address acts as unique identifier.
		*/
		std::map<std::string, int> clients;

		/**
		 * @brief Constructor
		 * 
		 * Default constructor, creates an empty list of clients.
		*/
		Configuration();

		/**
		 * @brief JSON Constructor
		 * 
		 * Constructor the takes a JSON file as input to populate the list of clients.
		 * See the project assets for a sample file.
		 * @param [in] filename Path to the JSON configuration file
		*/
		Configuration(std::string filename);

		/**
		 * @brief Destructor
		 * 
		 * Default destructor, empties the map.
		*/
		~Configuration();
	};
}
