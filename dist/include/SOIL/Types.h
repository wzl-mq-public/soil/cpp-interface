/**
 * @brief Type definitions
 *
 * Set of type definitions to make sure the user can name SOIL specific types without
 * knowing the explicit underlying C++ type.
*/

#pragma once
#include "Time.h"
#include "Enum.h"
namespace SOIL
{
	/**
	 * @brief SOIL Integer
	 * 
	 * Type definition for integer type used in SOIL.
	*/
	typedef int64_t INT;

	/**
	 * @brief SOIL Double
	 * 
	 * Type definition for double type used in SOIL.
	 * Double is the preferred floating point type in SOIL.
	*/
	typedef double DOUBLE;

	/**
	 * @brief SOIL Boolean
	 * 
	 * Type definition for boolean type used in SOIL.
	*/
	typedef bool BOOL;

	/**
	 * @brief SOIL String
	 * 
	 * Type definition for string type used in SOIL.
	*/
	typedef std::string STRING;

	/**
	 * @brief SOIL Enum
	 * 
	 * Type definition for the custom defined Enum in SOIL for consistency.
	*/
	typedef Enum ENUM;
	
	/**
	 * @brief SOIL Time
	 *
	 * Type definition for the custom defined Time in SOIL for consistency.
	*/
	typedef Time TIME;

	/**
	 * @brief SOIL Dimension
	 * 
	 * Type definition for the underlying datatype that is used to manage dimensions in SOIL
	*/
	typedef std::vector<unsigned int> DIMENSION;


}
