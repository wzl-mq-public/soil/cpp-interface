#pragma once
#include "constants.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <locale>
namespace SOIL
{
	/**
	 * @brief SOIL Time
	 * 
	 * Speial Time class for SOIL to implement the necessary functionality.
	*/
	class Time
	{
	private:
		/**
		 * @brief Null Flag
		 * 
		 * Internal flag whether the time is actually not set, i.e. null.
		*/
		bool _null;

		/**
		 * @brief Internal time representation
		 * 
		 * Internal time representation, which is realized by means of boost's posix time.
		 * 
		 * @todo: Check if dependency on boost can be eliminated with future versions of the C++ standard.
		*/
		boost::posix_time::ptime timestamp;
	public:
		/**
		 * @brief RFC3339 representation
		 * 
		 * Return a RFC3339 conformant representation of the time, e.g. 2021-06-07T22:01:23.078162Z as string.
		 * This is the main time representation used in SOIL.
		 * 
		 * @return RFC3339 string 
		*/
		DLL std::string rfc3339(void) const;
		
		/**
		 * @brief Uninitialized Constructor
		 * 
		 * Construct an uninitialized Time instance, where the @c _null flag is set to true. 
		*/
		DLL Time();

		/**
		 * @brief String Constructor
		 * 
		 * Constructs a Time object from an RFC3339 string.
		 * If an empty string is passed, the @c _null flag is set to true.
		 * 
		 * The exceptions from the underlying boost implementation are passed through.
		 * 
		 * @param [in] value RFC3339 string of the time to set. 
		*/
		DLL Time(std::string value);

		/**
		 * @brief Posix Time Construtor
		 * 
		 * Constructs a Time object from a @c boost::posix_time::ptime.
		 * This is useful in conjuction with `now()`.
		 * 
		 * @param [in] value Time to set.
		*/
		DLL Time(boost::posix_time::ptime value);

		/**
		 * @brief Destructor
		 *
		 * Default Destructor, there is no unexpected behaviour.
		*/
		DLL ~Time();

		/**
		 * @brief Is Null?
		 * 
		 * Returns whether the time is set to null.
		 * 
		 * @return 
		*/

		/**
		 * @brief Is Null?
		 *
		 * Function that returns true if the current data is set to null and false else.
		 *
		 * @return Null flag
		*/
		inline bool is_null(void) const { return _null; }

		/**
		 * @brief Set null
		 *
		 * Function to set the null status of the data container
		 * @param [in] _null Boolean state flag
		*/
		inline void set_null(bool _null = true) { this->_null = _null; }

		/**
		 * @brief Current Time
		 * 
		 * Returns the current UTC time as @c boost::posix_time::ptime to allow for quick access
		 * in implementations using this library. Thanks to the corresponding constructor, this
		 * function can also be used in assignments.
		 * 
		 * @return Current UTC time
		*/
		static DLL  boost::posix_time::ptime utc_now(void);

		/**
		 * @brief Friend Operator >=
		 * 
		 * Friend declaration of the >= operator, needed for correcet implementation.
		*/
		friend  DLL bool operator>=(const Time& t1, const Time& t2);

		/**
		 * @brief Friend Operator <=
		 *
		 * Friend declaration of the <= operator, needed for correcet implementation.
		*/
		friend  DLL bool operator<=(const Time& t1, const Time& t2);

		/**
		 * @brief Bytewise serialization
		 * 
		 * Bytewise serialization of the time for hashing purpose.
		 * The components are serialized in the following order:
		 * - uint16_t of the year
		 * - uint8_t of the month
		 * - uint8_t of the day
		 * - uint8_t of the hour
		 * - uint8_t of the minute
		 * - unit8_t of the seconds
		 * - uint32_t of the nanoseconds
		 *
		 * @return Serialized Bytestring 
		*/

		DLL std::vector<unsigned char> serialize(void) const;
	};

	/**
	 * @brief GEQ Time Operator
	 * 
	 * Greater or equal operation for two Time objects, which is needed for the correct
	 * implementation of the Range class.
	 * 
	 * Returns true if @c t1 >= @c t2, otherwise false. If one of the Time objects is set to null, the operation returns false.
	 * 
	 * @param t1 Left hand side time 
	 * @param t2 Right hand side time
	 * @return Comparison result as boolean
	*/
	DLL bool operator>=(const Time& t1, const Time& t2);


	/**
	 * @brief LEQ Time Operator
	 *
	 * Lower or equal operation for two Time objects, which is needed for the correct
	 * implementation of the Range class.
	 *
	 * Returns true if @c t1 <= @c t2, otherwise false. If one of the Time objects is set to null, the operation returns false.
	 *
	 * @param t1 Left hand side time
	 * @param t2 Right hand side time
	 * @return Comparison result as boolean
	*/
	DLL bool operator<=(const Time& t1, const Time& t2);
}