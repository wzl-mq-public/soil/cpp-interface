#pragma once
#include "constants.h"
#include "Types.h"
#define _TURN_OFF_PLATFORM_STRING
#include "cpprest/http_listener.h"

namespace SOIL
{
	/**
	 * @brief Value to JSON
	 * 
	 * Helper function to convert a value to its correct JSON value (RHS of key in document).
	 * This is a template function that is specialized for every datatype.
	 * 
	 * @tparam T Datatype under consideration
	 * @param[in] value Value to convert
	 * @return JSON value
	*/
	template <typename T>
	DLL web::json::value to_json(const T& value);

	/**
	 * @brief JSON to Value
	 *
	 * Helper function to convert a value from a JSON object to its equivalent internal SOIL datatype.
	 * This is a template function that is specialized for every datatype.
	 *
	 * @tparam T Datatype under consideration
	 * @param [in] json JSON object to convert
	 * @return Value in native datatype
	*/
	template <typename T>
	DLL T to_value(web::json::value json);


	template<>
	DLL web::json::value to_json<double>(const double& value);
	template<>
	DLL web::json::value to_json<int64_t>(const int64_t& value);
	template<>
	DLL web::json::value to_json<int>(const int& value);
	template<>
	DLL web::json::value to_json<std::string>(const std::string& value);
	template<>
	DLL web::json::value to_json<SOIL::TIME>(const SOIL::TIME& value);
	template<>
	DLL web::json::value to_json<SOIL::ENUM>(const SOIL::ENUM& value);
	template<>
	DLL web::json::value to_json<SOIL::BOOL>(const bool& value);
	template<>
	DLL double to_value<double>(web::json::value value);
	template<>
	DLL int to_value<int>(web::json::value value);
	template<>
	DLL int64_t to_value<int64_t>(web::json::value value);
	template<>
	DLL std::string to_value<std::string>(web::json::value json);
	template<>
	DLL SOIL::TIME to_value<SOIL::TIME>(web::json::value json);
	template<>
	DLL SOIL::ENUM to_value<SOIL::ENUM>(web::json::value json);
	template<>
	DLL bool to_value<bool>(web::json::value json);
}
