#pragma once
#include "constants.h"
#include "Element.h"
#include "Container.h"
#include "Range.h"
#include "Parameter.h"

namespace SOIL
{
	/**
	 * @brief Function Class
	 * 
	 * Class encapsulating a function represented in SOIL.
	 * The class directly inherits from Element.
	 * The recommended development pattern is to subclass this class and instantitate a use-case specific version.
	 * In this case, the @c invoke() method should be overriden to implement the function's business logic.
	 * 
	 * Arguments are represented using the Parameter class. A Function supports the HTTP GET (to introspect the signature)
	 * and HTTP POST (to execute the function) verbs.
	*/
	class Function :
		public Element
	{
	private:
		/**
		 * @brief List of arguments
		 * 
		 * List of arguments, which are implemented as parameters. To maintain the arguments,
		 * a map of shared pointers is used. Each argument has a locally unique identifier starting with PAR-
		 * which is used as identifier in the map.
		*/
		std::map<std::string, std::shared_ptr<Element>> arguments;

		/**
		 * @brief List of returns
		 *
		 * List of returns, which are implemented as parameters. To maintain the returns,
		 * a map of shared pointers is used. Each argument has a locally unique identifier starting with PAR-
		 * which is used as identifier in the map.
		*/
		std::map<std::string, std::shared_ptr<Element>> returns;
	public:
		/**
		 * @brief Constructor
		 * 
		 * Constructor that shall be used to instantiate a Function.
		 * If the class is subclassed, it shoule be called from the respective constructor.
		 * Most parameters are passed on to the constructor of Element.
		 * 
		 * @param [in] parent Shared pointer to parent object. Can be set to NULL for the creation of a root object
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		*/
		DLL Function(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology = "");

		/**
		 * @brief Destructor
		 * 
		 * Standard dsestructor
		*/
		DLL ~Function();

		/**
		 * @brief Create new Function
		 *
		 * Create a new Function using the default constructor and return a shared pointer reference.
		 * This is the preferred method for manual creation with consistent lifecycle handling.
		 *
		 * @param [in] parent Shared pointer to parent object. Can be set to NULL for the creation of a root object
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		*/
		static std::shared_ptr<Function> create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology = "");

		/**
		 * @brief Add argument
		 * 
		 * Add an argument when building the function. This method shoukd typically be called fron the
		 * subclass constructor. It uses the same templating system than Container does to handle multidimensional data.
		 * @tparam T Type of the data.
		 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
		 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] range Allowed range for this argument, which is checked prior to execution. Defaults to no range restriction.
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed (default)
		*/
		template<typename T, int x = -1, int y = -1>
		void add_argument(std::string uuid, std::string name, std::string description, std::string unit, SOIL::Range<T> range = SOIL::Range<T>(), std::string ontology = "");
		/**
		 * @brief Add argument with default value
		 *
		 * Add an argument when building the function with a default value. This method shoukd typically be called fron the
		 * subclass constructor. It uses the same templating system than Container does to handle multidimensional data.
		 * @tparam T Type of the data.
		 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
		 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] range Allowed range for this argument, which is checked prior to execution. Defaults to no range restriction.
		 * @param [in] default Default value provided as Container
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed (default)
		*/
		template<typename T, int x = -1, int y =-1>
		void add_argument(std::string uuid, std::string name, std::string description, std::string unit, SOIL::Range<T> range, const Container<T, x, y>& default_value, std::string ontology = "");
		
		/**
		 * @brief Add return value
		 *
		 * Add areturn value when building the function. This method shoukd typically be called fron the
		 * subclass constructor. It uses the same templating system than Container does to handle multidimensional data.
		 * @tparam T Type of the data.
		 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
		 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] range Allowed range for this argument. Defaults to no range restriction.
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed (default)
		*/
		template<typename T, int x = -1, int y = -1>
		void add_return(std::string uuid, std::string name, std::string description, std::string unit, SOIL::Range<T> range = SOIL::Range<T>(), std::string ontology = "");

		
		/**
		 * @brief Make JSON Return
		 * 
		 * Helper function that converts a return value provided as Container to a correpsonding JSON object.
		 * This function is usually useful in the custom implementation of @c invoke().
		 * 
		 * @tparam T Type of the data.
		 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
		 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
		 * @param [in] uuid Locally Unique identifier of return value
		 * @param [in] value Value of the return value
		 * @return JSON object corresponding to SOIL representation
		*/
		template<typename T, int x = -1, int y = -1>
		HTTP::Json make_return(std::string uuid, Container<T,x,y> value);

		/**
		 * @brief Make Argument from JSON
		 *
		 * Helper function that converts a HTTP Json object to a container of the passed value
		 * This function is usually useful in the custom implementation of @c invoke().
		 *
		 * @tparam T Type of the data.
		 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
		 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
		 * @param [in] uuid Locally Unique identifier of return value
		 * @param [in] external_json JSON object from which to convert
		 * @return Value as Container
		*/
		template<typename T, int x = -1, int y = -1>
		Container<T, x, y> make_argument(std::string uuid, HTTP::Json external_json);
		
		/**
		 * @brief HTTP JSON
		 *
		 * Get a HTTP JSON object corresponding to the function including arguments and returns
		 * @return JSON object
		*/
		DLL HTTP::Json wjson(void);

		/**
		 * @brief Handle HTTP GET request
		 * 
		 * On HTTP GET, the function returns information about itself and its signature.
		 * It is not invoked and this call should not lead to side effects.
		 * @param [in] request Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		DLL virtual HTTP::Response handle_get(HTTP::Request request, std::smatch match = std::smatch());

		/**
		 * @brief Handle HTTP POST request
		 *
		 * On HTTP POST, the function is invoked. With the POST request, a list of arguments must be passed
		 * in the request's body as list under the keyword "arguments".
		 * This method calls `invoke()`. If you do not implement this function, nothing will happen.
		 * 
		 * @param [in] request Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		DLL virtual HTTP::Response handle_post(HTTP::Request request, std::smatch match = std::smatch());

		/**
		 * @brief Core Invocation
		 * 
		 * This is the main function that @b MUST be implemented in a derived class to meaningful 
		 * act as a function. The arguments are passed as raw HTTP::Json objects mapped to their UUIDs.
		 * A map of HTTP::Json objects is expected as returns. It is recommend to use @c make_arguments and 
		 * @c make_returns in this function. Arguments and returns are not further decomposed to Containers to support
		 * multi-typed values.
		 * 
		 * @post This function will be called from the HTTP POST handler. Any exception thrown will be caught there again
		 * and lead to an HTTP Internal Error (500) return code.
		 * 
		 * @exception std::logic_error If not implemented in the derived class, the implementation here throws an exception.
		 * 
		 * @param [in] arguments Map of arguments as HTTP Json objects indexrd by their UUID
		 * @return Map of return values as HTTP Json objects indexrd by their UUID
		*/
		DLL virtual std::map<std::string, HTTP::Json> invoke(std::map<std::string, HTTP::Json> arguments);

		/**
		 * @brief Get Pointer
		 * 
		 * Return a shared pointer casted to the Function type to element itself.
		 * @return Casted pointer
		*/
		DLL inline std::shared_ptr<Function> ptr(void)
		{
			return std::dynamic_pointer_cast<Function>(Element::self);
		}
	};
	template<typename T, int x, int y>
	void Function::add_argument(std::string uuid, std::string name, std::string description, std::string unit, SOIL::Range<T> range, std::string ontology)
	{
		arguments[uuid].reset(new Parameter<T, x, y>(Element::self, uuid, name, description, unit, false, ontology, range));
	}
	template<typename T, int x, int y>
	void Function::add_argument(std::string uuid, std::string name, std::string description, std::string unit,  SOIL::Range<T> range, const Container<T,x,y>& default_value, std::string ontology)
	{
		Parameter<T, x, y>* par = new Parameter<T, x, y>(Element::self, uuid, name, description, unit, false, ontology, range);
		arguments[uuid].reset(par);
		*par = default_value;
	}


	template<typename T, int x,int y>
	void Function::add_return(std::string uuid, std::string name, std::string description, std::string unit, SOIL::Range<T> range, std::string ontology)
	{
		returns[uuid].reset(new Parameter<T, x, y>(Element::self, uuid, name, description, unit, false, ontology,  range));
	}
	template<typename T, int x, int y>
	HTTP::Json Function::make_return(std::string uuid, Container<T, x, y> value)
	{
		HTTP::Json json_root = HTTP::Json::object();
		json_root[_XPLATSTR("uuid")] = to_json<std::string>(uuid);
		json_root[_XPLATSTR("value")] = value.json()[_XPLATSTR("value")];
		return json_root;
	}
	template<typename T, int x, int y>
	Container<T, x, y> Function::make_argument(std::string uuid, HTTP::Json external_json)
	{
		for (auto& a : external_json.as_array())
		{
			if (to_value<std::string>(a[_XPLATSTR("uuid")]) == uuid)
			{
				return Container<T, x, y>(a[_XPLATSTR("value")]);
			}
		}
		return Container<T, x, y>();
	}
}

