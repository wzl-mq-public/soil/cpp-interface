#pragma once
#include "constants.h"
#include "REST/Types.h"
#include "json_helpers.h"
#include "Range.h"
namespace SOIL
{
	/**
	 * @brief Data Container
	 * 
	 * Container class to manage multidmensional data of (nearly) arbitrary type.
	 * The data can be scalar, 1D or 2D while the size of dimensions is a priori unknown.
	 * This class then provides an abstratcion layer to the other elements of SOIL to allow for consistent implementation.
	 * Therefor it makes heavy use of templates, which are specialized for certain cases where @c x and/or @c y are -1 and for special types.
	 * 
	 * @tparam T Type of the data.
	 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
	 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
	*/
	template <typename T, int x=-1, int y=-1>
	class Container
	{
	private:
		/**
		 * @brief Internal data
		 * 
		 * internal data buffer that can hold up to two dimensions
		*/
		std::vector<std::vector<T>> data;

		/**
		 * @brief Null flag
		 * 
		 * Any data item in SOIL can be set to null to express that no data is present at all.
		 * To realize this internally indepedent of the data type, this boolean flag is used.
		*/
		bool _null;
	public:
		/**
		 * @brief Empty constructor
		 * 
		 * Constructs an empty container.
		 * _null will be set to true as no data has been added. 
		 * @exception std::runtime_error An exception is thrown if an invalid combination of template parameters is used.
		*/
		Container();

		/**
		 * @brief Data copy constructor
		 * 
		 * Constructor that initializes the data with the given value.
		 * @param [in] value Data for initialization, wich must match in type and dimension
		*/
		Container(const std::vector<std::vector<T>>& value);

		/**
		 * @brief JSON Constructor
		 * 
		 * Constructor that takes dimension and value from an JSON representation adhering to the SOIL nomenclature.
		 * @param [in] json JSON object to parse
		*/
		Container(HTTP::Json json);

		/**
		 * @brief Deferencing operator
		 * 
		 * Return a copy of the raw data, similar to other uses of * in the standard library.
		 * @return Copy of underlying data.
		*/
		std::vector<std::vector<T>> operator*(void) const;

		/**
		 * @brief Is Null?
		 * 
		 * Function that returns true if the current data is set to null and false else.
		 * 
		 * @return Null flag
		*/
		inline bool is_null(void) const {return _null;}

		/**
		 * @brief Set null
		 * 
		 * Function to set the null status of the data container
		 * @param [in] _null Boolean state flag
		*/
		inline void set_null(bool _null = true) { this->_null = _null; }

		/**
		 * @brief WJSON representation
		 * 
		 * Get a JSON object respresentation of the container's current data.
		 * The naming @c wjson() is chosen as it is consistently used through the library when using wide-string JSON as introduced by cpprestsdk.
		 * 
		 * @return JSON object. 
		*/
		HTTP::Json wjson(void);

		/**
		 * @brief Check range
		 * 
		 * Check whether the data of the container lies within the specified range.
		 * In the case of multidimensional data, this check is carried out on all elememts and only returns true if it is applicable to all.
		 * @param [in] range Range for which to check
		 * @return Check result as boolean
		*/
		bool check_range(Range<T> range) const;

		/**
		 * @brief Data Accessor
		 * 
		 * STL-style data accesor retrieving a a single element from multidimensional-data.
		 * @param [in] i Index position along first dimension 
		 * @param [in] j Index position along second dimension
		 * @return Data value of type T
		*/
		T& at(int i, int j);

		/**
		 * @brief Serialize value
		 * 
		 * Serialize the value to a bytestring for hashing purposes
		 * @return Bytestring of serialization 
		*/
		std::vector<unsigned char> serialize_value(void) const;

		/**
		 * @brief Serialize dimensions
		 *
		 * Serialize the dimensions to a bytestring for hashing purposes
		 * @return Bytestring of serialization
		*/
		std::vector<unsigned char> serialize_dimensions(void) const;

	};

	/**
	 * @brief Template specialization for 1D-Arrays/Vectors
	 *
	 * This is the specialization of the templated class for data that expands along one dimension.
	 * For the documentation of methods, please refer to the unpecialized class.
	 * @tparam T Type of the data
	 * @tparam x First dimension of the data
	*/
	template <typename T, int x>
	class Container<T, x, -1>
	{
	private:
		std::vector<T> data;
		bool _null;
	public:
		Container();
		Container(const std::vector<T>& value);
		Container(HTTP::Json json);
		std::vector<T> operator*(void) const;
		inline bool is_null(void) const { return _null; }
		inline void set_null(bool _null = true) { this->_null = _null; }
		HTTP::Json wjson(void);
		bool check_range(Range<T> range) const;
		T& at(int i);
		std::vector<unsigned char> serialize_value(void) const;
		std::vector<unsigned char> serialize_dimensions(void) const;

	};

	/**
	 * @brief Template specialization for Scalars
	 *
	 * This is the specialization of the templated class for scalar data.
	 * For the documentation of methods, please refer to the unpecialized class.
	 * @tparam T Type of the data
	*/
	template <typename T>
	class Container<T, -1, -1>
	{
	private:
		T data;
		bool _null;
	public:
		Container();
		Container(const T& value);
		Container(HTTP::Json json);
		T operator*(void) const;
		inline bool is_null(void) const { return _null; }
		inline void set_null(bool _null = true) { this->_null = _null; }
		HTTP::Json wjson(void);
		bool check_range(Range<T> range) const;
		T& at(void);
		std::vector<unsigned char> serialize_value(void) const;
		std::vector<unsigned char> serialize_dimensions(void) const;

	};


	template<typename T, int x, int y>
	Container<T, x, y>::Container()
	{
		if ((x < -1) || (y < -1))
		{
			throw std::runtime_error("Invalid Dimension smaller than -1");
		}
		if ((x == -1) && (y > -1))
		{
			throw std::runtime_error("An object can only be one-dimensional in its first dimension!");
		}
		_null = true;
	}

	template<typename T, int x, int y>
	Container<T, x, y>::Container(const std::vector<std::vector<T>>& value)
	{
		if ((x != 0) && (value.size() != x))
		{
			throw std::runtime_error("Invalid dimension in assignment!");
		}
		if (y != 0)
		{
			for (auto v : value)
			{
				if (v.size() != y)
				{
					throw std::runtime_error("Invalid dimension in assignment!");
				}
			}
		}
		data = value;
		_null = false;
	}

	template<typename T, int x, int y>
	Container<T, x, y>::Container(HTTP::Json json)
	{
		std::vector<std::vector<T>> value;
		int i = 0;
		for (auto& vx : json.as_array())
		{
			value.push_back(std::vector<T>());
			i++;
			for (auto vy : vx.as_array())
			{
				value.at(i).push_back(to_value<T>(vy));
			}
		}
		if ((x != 0) && (value.size() != x))
		{
			throw std::runtime_error("Invalid dimension in assignment!");
		}
		if (y != 0)
		{
			for (auto v : value)
			{
				if (v.size() != y)
				{
					throw std::runtime_error("Invalid dimension in assignment!");
				}
			}
		}
		data = value;
		_null = false;
	}

	template<typename T, int x, int y>
	std::vector<std::vector<T>> Container<T, x, y>::operator*(void) const
	{
		return data;
	}

	template<typename T, int x, int y>
	HTTP::Json Container<T, x, y>::wjson(void)
	{
		HTTP::Json json_root = HTTP::Json();
		json_root[_XPLATSTR("dimension")] = HTTP::Json::array({ x, y });
		if (is_null())
		{
			json_root[_XPLATSTR("value")] = HTTP::Json::null();
		}
		else
		{
			json_root[_XPLATSTR("value")] = HTTP::Json::array();
			for (int i = 0; i < static_cast<int>(data.size()); i++)
			{
				json_root[_XPLATSTR("value")][i] = HTTP::Json::array();
				for (int j = 0; j < static_cast<int>(data.at(i).size()); j++)
				{
					json_root[_XPLATSTR("value")][i][j] = to_json<T>(data.at(i).at(j));
				}
			}
		}
		return json_root;
	}

	template<typename T, int x, int y>
	bool Container<T, x, y>::check_range(Range<T> range) const
	{
		if (is_null())
		{
			return true;
		}
		for (auto vx : data)
		{
			for (auto vy : vx)
			{
				if (!range.check(vy))
				{
					return false;
				}
			}
		}
		return true;
	}

	template<typename T, int x, int y>
	T& Container<T, x, y>::at(int i, int j)
	{
		if (is_null())
		{
			throw std::logic_error("Value is NULL");
		}
		return data.at(i).at(j);
	}

	template<typename T, int x, int y>
	std::vector<unsigned char> Container<T, x, y>::serialize_value(void) const
	{
		if (is_null())
		{
			throw std::logic_error("Value is NULL");
		}
		int current_x = static_cast<int>(data.size());
		int current_y = current_x > 0 ? static_cast<int>(data.at(0).size()) : 0;
		size_t size = current_x * current_y * sizeof(T);
		std::vector<unsigned char> serialization;
		serialization.reserve(size);
		
		
		for (int i = 0; i < current_x ; i++)
		{
			{
				const unsigned char* pointer = reinterpret_cast<const unsigned char*>(data.at(i).data());
				for (int j = 0; j < static_cast<int>(current_y*sizeof(T)); j++)
				{
					serialization.push_back(pointer[j]);
				}
			}
		}
		return serialization;
	}

	template<typename T, int x, int y>
	std::vector<unsigned char> Container<T, x, y>::serialize_dimensions(void) const
	{
		size_t size = sizeof(uint8_t) + 2 * sizeof(uint32_t);
		std::vector<unsigned char> serialization;
		serialization.reserve(size);

		uint8_t n_dimensions = 2;
		uint32_t current_x = data.size();
		uint32_t current_y = current_x > 0 ? data.at(0).size : 0;

		serialization.push_back(n_dimensions);

		unsigned char* current_x_pointer = reinterpret_cast<unsigned char*>(&current_x);
		unsigned char* current_y_pointer = reinterpret_cast<unsigned char*>(&current_y);

		for (int i = 0; i < sizeof(uint32_t); i++)
		{
			serialization.push_back(current_x_pointer[i]);
		}
		for (int i = 0; i < sizeof(uint32_t); i++)
		{
			serialization.push_back(current_y_pointer[i]);
		}

		return serialization;
	}

	template<typename T, int x>
	Container<T, x, -1>::Container()
	{
		if (x < -1) 
		{
			throw std::runtime_error("Invalid dimension in assignment!");
		}
		_null = true;
	}

	template<typename T, int x>
	Container<T, x, -1>::Container(const std::vector<T>& value)
	{
		if ((x != 0) && (value.size() != x))
		{
			throw std::runtime_error("Invalid dimension in assignment!");
		}
		data = value;
		_null = false;
	}

	template<typename T, int x>
	Container<T, x, -1>::Container(HTTP::Json json)
	{
		std::vector<T> value;
		for (auto& v : json.as_array())
		{
			value.push_back(to_value<T>(v));
		}
		if ((x != 0) && (value.size() != x))
		{
			throw std::runtime_error("Invalid dimension in assignment!");
		}
		data = value;
		_null = false;
	}


	template<typename T, int x>
	std::vector<T> Container<T, x, -1>::operator*(void) const
	{
		return data;
	}

	template<typename T, int x>
	HTTP::Json Container<T, x, -1>::wjson(void)
	{
		HTTP::Json json_root = HTTP::Json();
		json_root[_XPLATSTR("dimension")] = HTTP::Json::array();
		json_root[_XPLATSTR("dimension")][0] = HTTP::Json::number(x);
		if (is_null())
		{
			json_root[_XPLATSTR("value")] = HTTP::Json::null();
		}
		else
		{
			json_root[_XPLATSTR("value")] = HTTP::Json::array();
			for (int i = 0; i < static_cast<int>(data.size()); i++)
			{
				json_root[_XPLATSTR("value")][i] = to_json<T>(data.at(i));
			}
		}
		return json_root;
	}

	template<typename T, int x>
	bool Container<T, x, -1>::check_range(Range<T> range) const
	{
		if (is_null())
		{
			return true;
		}
		for (auto vx : data)
		{
			if (!range.check(vx))
			{
				return false;
			}
		}
		return true;
	}

	template<typename T, int x>
	T& Container<T, x, -1>::at(int i)
	{
		if (is_null())
		{
			throw std::logic_error("Value is NULL");
		}
		return data.at(i);
	}

	template<typename T, int x>
	std::vector<unsigned char> Container<T, x, -1>::serialize_value(void) const
	{
		if (is_null())
		{
			throw std::logic_error("Value is NULL");
		}
		int current_x = static_cast<int>(data.size());
		size_t size = current_x * sizeof(T);
		std::vector<unsigned char> serialization;
		serialization.reserve(size);

		const unsigned char* pointer = reinterpret_cast<const unsigned char*>(data.data());
		for (int i = 0; i < static_cast<int>(current_x * sizeof(T)); i++)
		{
			serialization.push_back(pointer[i]);
		}
		return serialization;
	}

	template<typename T, int x>
	std::vector<unsigned char> Container<T, x, -1>::serialize_dimensions(void) const
	{
		size_t size = sizeof(uint8_t) + sizeof(uint32_t);
		std::vector<unsigned char> serialization;
		serialization.reserve(size);

		uint8_t n_dimensions = 1;
		uint32_t current_x = static_cast<uint32_t>(data.size());

		serialization.push_back(n_dimensions);
		unsigned char* current_x_pointer = reinterpret_cast<unsigned char*>(&current_x);

		for (int i = 0; i < sizeof(uint32_t); i++)
		{
			serialization.push_back(current_x_pointer[i]);
		}
		return serialization;
	}

	template<typename T>
	Container<T, -1, -1>::Container()
	{
		_null = true;
	}

	template<typename T>
	Container<T, -1, -1>::Container(const T& value)
	{
		data = value;
		_null = false;
	}

	template<typename T>
	Container<T, -1, -1>::Container(HTTP::Json json)
	{
		T value = to_value<T>(json);
		data = value;
		_null = false;
	}

	template<typename T>
	T Container<T, -1, -1>::operator*(void) const
	{
		return data;
	}

	template<typename T>
	HTTP::Json Container<T, -1, -1>::wjson(void)
	{
		HTTP::Json json_root = HTTP::Json();
		json_root[_XPLATSTR("dimension")] = HTTP::Json::array();
		if (is_null())
		{
			json_root[_XPLATSTR("value")] = HTTP::Json::null();
		}
		else
		{
			json_root[_XPLATSTR("value")] = to_json<T>(data);
		}
		return json_root;
	}

	template<typename T>
	bool Container<T, -1, -1>::check_range(Range<T> range) const
	{
		if (is_null())
		{
			return true;
		}
		return range.check(data);
	}

	template<typename T>
	T& Container<T, -1, -1>::at(void)
	{
		if (is_null())
		{
			throw std::logic_error("Value is NULL");
		}
		return data;
	}

	template<typename T>
	std::vector<unsigned char> Container<T, -1, -1>::serialize_value(void) const
	{
		if (is_null())
		{
			throw std::logic_error("Value is NULL");
		}
		size_t size =  sizeof(T);
		std::vector<unsigned char> serialization;
		serialization.reserve(size);

		const unsigned char* pointer = reinterpret_cast<const unsigned char*>(&data);
		for (int i = 0; i <  sizeof(T); i++)
		{
			serialization.push_back(pointer[i]);
		}
		return serialization;
	}

	template<typename T>
	std::vector<unsigned char> Container<T, -1, -1>::serialize_dimensions(void) const
	{
		size_t size = sizeof(uint8_t);
		std::vector<unsigned char> serialization;
		serialization.reserve(size);

		uint8_t n_dimensions = 0;

		serialization.push_back(n_dimensions);
		return serialization;
	}
}