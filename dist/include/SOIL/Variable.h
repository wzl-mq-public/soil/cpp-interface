#pragma once
#include "constants.h"
#include "Types.h"
#include "Element.h"
#include "Container.h"
#include "Range.h"
#include "Figure.h"
#include "SIGN/Signer.h"
#include "SIGN/Hasher.h"
#include "MQTT/Publisher.h"
#include <boost/algorithm/string/join.hpp>


namespace SOIL
{
	/**
	 * @brief Variable Class
	 *
	 * This class represents a SOIL Variable. Parameter and Variable share many common properties
	 * and therefore both inherit from Figure, such that methods there should be considered in any case.
	 * Variables are primarily intended for values that  represent any measurement or physical phenomenon outside the immediate control of device.
	 * In contrast to parameter, they cannot be set externally and may not be constant. In addition, they contain a custom message (nonce), a hash (for traceability purposes)
	 * and a covariance property. The covariance should represent uncertainty as multivariate normal distribution with coverage factor of 1.
	 * 
	 * Covariance is currently not supported for 2-dimensional data (i.e. which would lead to a 4-dimensional covariance expression)
	 * 
	 * In lightweight scenarios, this class may be instantiated directly, for more specific scenarios, it shouls be subclassed and override the implementation
	 * of `read()`.
	 *
	 * A parameter supports HTTP GET (read) and HTTP OPTIONS (read without updating value) verbs.
	 *
	 * The data management of Figure and hence Parameter relies on Container, such that the same templating logic is used.
	 *
	 * @tparam T Type of the data.
	 * @tparam x First dimension of the data. -1 means unused, 0 means arbitray size. Cannot be -1 if @c y is not -1.
	 * @tparam y Second dimension of the data. -1 means unused, 0 means arbitray size. Must be -1 for @c x to be -1.
	 *
	 * @todo The HTTP handlers may be moved to protected if HTTP::Server is declared as friend class. Currently this is not done to alllow for greater flexibility.
	*/
	template <typename T, int x = -1, int y = -1>
	class Variable : public Figure<T, x, y>
	{
	protected:

		/**
		 * @brief Nonce
		 * 
		 * Nonce with is considered as arbitrary message that the user may add to the measurement.
		 * This could be JOB-IDs or similar.
		*/
		std::string nonce;

		/**
		 * @brief Checking Hash
		 * 
		 * Hash that is calculated from a defined binary representation of the variable and may be signed with private_key
		 * such that the integrity of the data can be verified. Is null if not explicitly set.
		 * 
		 * @todo This is still error prone across different languages and platforms as endianess and memory layout need to be considered.
		*/
		std::vector<unsigned char> hash;

		/**
		 * @brief Covariance of the value
		 * 
		 * Covariance of the value expressed as multidimensional normal distribution with coverage factor 1.
		 * The dimensionality depends on the dimensionality of the value:
		 * - For scalar data, the covariance is a scalar corresponding to the variance.
		 * - For data expanding along one dimension with length @em n the covariance is a matrix with dimension @em (n,n)
		 * - For data expanding along two dimensions, covariance is currently not supported.
		 * 
		 * Note that the second template argument of the Container is @c x in this case.
		*/
		Container<T, x, x>  covariance;

		/**
		 * @brief Read callback
		 *
		 * Read callback that can be implemented by deriving classes to perform custom build logic on read actions, e.g. update the value from an external storage.
		 * Be careful when including long-running queries as they will block the HTTP call.
		 * Declared virtual to make sure the derived method is called first. Does nothing be default.
		*/
		virtual void read(void);
		
		/**
		 * @brief Write callback
		 * 
		 * This is an empty implementation of the `write()` function which is needed to avoid
		 * that Variable becomes abstract. It is never called.
		 *  
		*/
		virtual void write(void);

	public:
		/**
		 * @brief Constructor
		 *
		 * Standard constructor intialiazing the values. If subclassed, it should be called from the subclass constructor.
		 * @param [in] parent Shared pointer to parent object.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 * @param [in] range Allowed range for the variable values, defaults to an empty object, i.e. allowing all values
		 * @param [in] time Timestamp for the initial value, defaults to unset
		 * @param [in] nonce Custom message to add to the variable, defaults to unset
		*/
		Variable(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, std::string ontology = "", Range<T> range = Range<T>(), TIME time = TIME(), std::string nonce = "");
		
		/**
		 * @brief Destructor
		 *
		 * Default destructor, without custom effort.
		*/
		~Variable();

		/**
		 * @brief Create new Variable
		 *
		 * Create a new Variable using the default constructor and return a shared pointer reference.
		 * This is the preferred method for manual creation with consistent lifecycle handling.
		 * @param [in] parent Shared pointer to parent object.
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] unit UNECE unit code, e.g. MTR
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		 * @param [in] range Allowed range for the variable values, defaults to an empty object, i.e. allowing all values
		 * @param [in] time Timestamp for the initial value, defaults to unset
		 * @param [in] nonce Custom message to add to the variable, defaults to unset
		*/
		static std::shared_ptr<Variable> create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, std::string ontology = "", Range<T> range = Range<T>(), TIME time = TIME(), std::string nonce = "");

		/**
		 * @brief Assignment operator
		 *
		 * Assigns the value provided as container on the right hand side to the variable.
		 * Immediately resorts to the implementation in Figure internally.
		 *
		 * @exception std::range_error Throws an exception if the value to assign is outside the allowed range.
		 *
		 * @param[in] value Value to assign
		 * @return Reference to the current Variable
		*/
		Variable<T, x, y>& operator =(const Container<T, x, y>& value);

		/**
		 * @brief HTTP JSON
		 *
		 * Get a HTTP JSON object corresponding to the current state of the Parameter.
		 * This function provides a SOIL-conformant JSON representation the parameter.
		 * It internally extends the method of Figure.
		 *
		 * @return JSON object
		*/
		HTTP::Json wjson(void) override;

		/**
		 * @brief HTTP GET Handler
		 *
		 * Handler that is called by the server on HTTP requests on a GET method.
		 * This function returns a representation of the Variable and its current value to the requesting party.
		 * It should not be overridden directly in subclasses, instead the `read()` function should be overriden.
		 * It is an expected behaviour that `read()` calls an external method to update the value and may take some time to return.
		 * Please note that the default timeout in cpprestsdk is 120s, which is already very long. Consider an asynchronous update model if your
		 * devices takes longer to measure.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_get(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief HTTP OPTIONS Handler
		 *
		 * Handler that is called by the server on HTTP requests on a OPTIONS method.
		 * This function returns a representation of the Variable and its current stored value to the requesting party.
		 * In contrast to GET, the `read()` function is not called such that there are no side effects if a requesting party
		 * just wants to obtain informtion about the variable's metadata in the first place.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle_options(HTTP::Request message, std::smatch match = std::smatch()) override;

		/**
		 * @brief Update value
		 * 
		 * Update the value of the variable. As value, time and nonce are expected to change with each value update,
		 * the three components are passed at the same time.
		 * 
		 * @param [in] value Value to set
		 * @param [in] time Timestamp to set as corresponidng to the value
		 * @param [in] nonce Nonce to set as corresponding to the value
		*/
		void update(const Container<T, x, y>& value, TIME time, std::string nonce = "");

		/**
		 * @brief Set Covariance
		 * 
		 * Set the covariance corresponding to the value. This is a separate call as the following three scenarios are foreseen:
		 * - The covariance is set in the user defined update cycle, so this call can directly follow `update()`.
		 * - The covariance is estimated constant for all measurements, so there is no need for updating it
		 * - The covariance is not available, thus set to null.
		 * 
		 * @param [in] covariance Covariance provided as container object
		*/
		void set_covariance(Container<T, x, x>  covariance);

		/**
		 * @brief Get bytewise representation
		 * 
		 * Get a bytewise representation of the variable. This is constructed in the following order:
		 * - Dimensions in declared order
		 * - Value in row-major order
		 * - Covariane in row-major-order (cannot be null!)
		 * - Unit directly taken from UTF-8 chars
		 * - Timestamp (cf. explanation there)
		 * - Nonce as UTF-8 string
		 *
		 * This function is mainly used for hashing and signing purposes.
		 * It does not write to an internal attribute.
		 * 
		 * @return Constructucted bytestring
		*/
		std::vector <unsigned char> bytes(void);

		/**
		 * @brief Calculate SHA256
		 *
		 * Calculates the sha256 hash of the bytestring representation.
		 * Internally calls `bytes()`, but does not write to any value.
		 *
		 * @return  SHA256 hash as bytestring
		*/
		std::vector <unsigned char> sha256(void);

		/**
		 * @brief Sign the variable data
		 * 
		 * Sign the measurement data. If an pointer to an instance of SIGN::Signer is passed, the `fingerprint()` function is used to sign
		 * the sha256 hash with the managed private key. Otherwise, the `sha256()` function is used.
		 * 
		 * In both cases, the resulting bytestring is stored to the internal `hash` attribute and returned.
		 * 
		 * @pre The SIGN::Signer instance must be instialized elsewhere and have an apropriate lifecycle.
		 * @post The `hash` attribute is automatically updated by this call.
		 * 
		 * @param [in] signer Reference to a SIGN::Signer object holding the private key to use 
		 * @return Signature bytestring
		*/
		std::vector <unsigned char> sign(std::shared_ptr<SIGN::Signer> signer = NULL);

		/**
		 * @brief Calculate RSA fingerprint 
		 *
		 * Signs the sha256 hash with the private key managed by the passed instance of SIGN::Signer. 
		 * Does not write to an internal attribute.
		 *
		 * @pre The SIGN::Signer instance must be instialized elsewhere and have an apropriate lifecycle.
		 *
		 * @param [in] signer Reference to a SIGN::Signer object holding the private key to use
		 * @return Signature bytestring
		*/
		std::vector <unsigned char> fingerprint(std::shared_ptr<SIGN::Signer> signer);

		
		/**
		 * @brief Get Pointer
		 *
		 * Return a shared pointer casted to the Parameter type to element itself.
		 * @return Casted pointer
		*/
		inline std::shared_ptr<Variable> ptr(void)
		{
			return std::dynamic_pointer_cast<Variable>(Element::self);
		}

		/**
		 * @brief Publish to MQTT
		 *
		 * Publish the current JSON representation under the FQID as topic using a given MQTT publisher.
		 * This function must be explicitly called from the user's code as otherwise the update cycle would
		 * depend on the publisher and the user will be left without control to call other methods before publishing.
		 *
		 * A good pattern is to have a reference to an MQTT publisher in a sublcassing implementation and implement a complete update
		 * cycle there.
		 *
		 * @pre An MQTT::Publisher must be instantiated elsewhere and have a valid lifecycle.
		 *
		 * @param [in] publisher Reference to the publisher to use
		 * @param [in] qos Quality of service to choose for MQTT message
		 * @param [in] retain Flag whether to retain the message on the server.
		 * @return Boolean flag whether the message was accepted in the message queue.
		 *
		 * @todo Currently the implementation of this function is redundant in Variable and Parameter.
		 * It is deliberately not moved to Figure as different implementations may occur in the future,
		 * but would be a valid option.
		*/
		bool mqtt(std::shared_ptr<MQTT::Publisher> publisher, int qos = 0, bool retain = false);
	};


}



template<typename T, int x, int y>
SOIL::Variable<T, x, y>::Variable(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, std::string ontology, Range<T> range, TIME time, std::string nonce) : SOIL::Figure<T, x, y>(parent, uuid, name, description, unit, ontology, range, time)
{
	if (uuid.substr(0, 3) != "VAR")
	{
		throw std::logic_error("UUIDs for Variables must start with VAR!");
	}

	HTTP::Resource::allowed_methods = { HTTP::Methods::GET, HTTP::Methods::GET, HTTP::Methods::OPTIONS, HTTP::Methods::HEAD};
}

template<typename T, int x, int y>
SOIL::Variable<T, x, y>::~Variable()
{
}

template<typename T, int x, int y>
inline std::shared_ptr<SOIL::Variable<T,x,y> > SOIL::Variable<T, x, y>::create(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string unit, std::string ontology, Range<T> range, TIME time, std::string nonce)
{
	Variable<T, x, y>* variable = new Variable<T, x, y>(parent, uuid, name, description, unit, ontology, range, time, nonce);
	return variable->ptr();
}

template<typename T, int x, int y>
inline SOIL::Variable<T, x, y>& SOIL::Variable<T, x, y>::operator=(const Container<T, x, y>& value)
{
	Figure<T, x, y>::operator=(value);
	return *this;
}


template<typename T, int x, int y>
HTTP::Json SOIL::Variable<T, x, y>::wjson(void)
{
	std::unique_lock<std::recursive_mutex> lock(Element::mutex);
	HTTP::Json json_root = SOIL::Figure<T, x, y>::wjson();

	
	std::ostringstream buffer;

	for (std::string::size_type i = 0; i < hash.size();i++)
	{
		buffer << std::hex << std::setfill('0') << std::setw(2) << std::uppercase  << (int)hash[i];
		if (i != hash.size() - 1)
		{
			buffer << std::setw(1) << " ";
		}
	}

	std::string readable_hash = buffer.str();
	
	json_root[_XPLATSTR("nonce")] = (nonce == "") ? HTTP::Json::null() : SOIL::to_json(nonce);
	json_root[_XPLATSTR("hash")] = (readable_hash == "") ? HTTP::Json::null() : SOIL::to_json(readable_hash);
	json_root[_XPLATSTR("covariance")] = covariance.wjson()[_XPLATSTR("value")];

	return json_root;
}

template<typename T, int x, int y>
void SOIL::Variable<T, x, y>::read(void)
{
}

template<typename T, int x, int y>
void SOIL::Variable<T, x, y>::write(void)
{
}

template<typename T, int x, int y>
inline void SOIL::Variable<T, x, y>::set_covariance(Container<T, x, x> covariance)
{
	std::unique_lock<std::recursive_mutex> lock(Element::mutex);
	this->covariance = covariance;
}

template<typename T, int x, int y>
inline std::vector<unsigned char> SOIL::Variable<T, x, y>::bytes(void)
{
	std::unique_lock<std::recursive_mutex> lock(Element::mutex);
	std::vector<unsigned char> result;
	std::vector<unsigned char> bytes_dimension = Figure<T, x, y>::value.serialize_dimensions();
	std::vector<unsigned char> bytes_value = Figure<T, x, y>::value.serialize_value();
	std::vector<unsigned char> bytes_covariance = covariance.serialize_value();
	std::vector<unsigned char> bytes_unit(3, ' ');
	std::vector<unsigned char> bytes_time = Figure<T,x,y>::time.serialize();
	std::vector<unsigned char> bytes_nonce;
	for (int i = 0; i < static_cast<int>(std::min(static_cast<size_t>(3), Figure<T,x,y>::unit.length())); i++)
	{
		bytes_unit.at(i) = Figure<T, x, y>::unit.at(i);
	}
	for (int i = 0; i < static_cast<int>(nonce.length()); i++)
	{
		bytes_nonce.push_back(nonce.at(i));
	}

	result.insert(result.end(), bytes_dimension.begin(), bytes_dimension.end());
	result.insert(result.end(), bytes_value.begin(), bytes_value.end());
	result.insert(result.end(), bytes_covariance.begin(), bytes_covariance.end());
	result.insert(result.end(), bytes_unit.begin(), bytes_unit.end());
	result.insert(result.end(), bytes_time.begin(), bytes_time.end());
	result.insert(result.end(), bytes_nonce.begin(), bytes_nonce.end());

	return result;
}

template<typename T, int x, int y>
inline std::vector<unsigned char> SOIL::Variable<T, x, y>::sha256(void)
{
	std::vector<unsigned char> data = this->bytes();
	return SIGN::Hasher::sha256(data.data(), data.size());
}

template<typename T, int x, int y>
inline std::vector<unsigned char> SOIL::Variable<T, x, y>::fingerprint(std::shared_ptr<SIGN::Signer> signer)
{
	return signer->sign(this->sha256());
}

template<typename T, int x, int y>
inline std::vector<unsigned char> SOIL::Variable<T, x, y>::sign(std::shared_ptr<SIGN::Signer> signer)
{
	if (signer != NULL)
	{
		this->hash = this->fingerprint(signer);
	}
	else {
		this->hash = this->sha256();
	}

	return this->hash;
}

template<typename T, int x, int y>
inline bool SOIL::Variable<T, x, y>::mqtt(std::shared_ptr<MQTT::Publisher> publisher, int qos, bool retain)
{
	std::string topic = boost::algorithm::join(this->fqid(), "/");
	return publisher->publish(topic, this->json(), qos, retain);
}


template<typename T, int x, int y>
inline HTTP::Response SOIL::Variable<T, x, y>::handle_get(HTTP::Request message, std::smatch match)
{
	this->read();

	HTTP::Response response;
	response.set_body(this->wjson());
	response.set_status_code(HTTP::Status::OK);

	return response;
}

template<typename T, int x, int y>
inline HTTP::Response SOIL::Variable<T, x, y>::handle_options(HTTP::Request message, std::smatch match)
{
	HTTP::Response response;
	response.set_body(this->wjson());
	response.set_status_code(HTTP::Status::OK);

	return response;
}

template<typename T, int x, int y>
inline void SOIL::Variable<T, x, y>::update(const Container<T, x, y>& value, TIME time, std::string nonce)
{
	Figure<T, x, y>::update(value, time);
	this->nonce = nonce;

	hash.clear();
	covariance.set_null(true);
}




