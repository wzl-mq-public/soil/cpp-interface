#pragma once
#include "constants.h"
#include "REST/constants.h"
#include "REST/Resource.h"
#include <memory>
#include <vector>
#include <map>
#include <mutex>
#include <regex>
#include <string>

namespace SOIL
{
	/**
	 * @brief SOIL Base Element
	 * 
	 * Element is the base class of Object, Function, Parameter and Variable in SOIL.
	 * It is the first class that inherits from HTTP::Resource.
	 * It contains the main implementation of the business logic for building the Element tree,
	 * i.e. managing child items and resolving absolute and relative parths. It also provides common
	 * base class which can be used for generic pointer in C++.
	*/
	class DLL Element : public HTTP::Resource
	{
	private:
	public:
		/**
		 * @brief Children Map
		 * 
		 * Map of child elements, which are referenced by means of shared pointers and identified by their local UUID in string form.
		*/
		std::map<std::string, std::shared_ptr<Element> > children;

		/**
		 * @brief Parent Pointer
		 * 
		 * Shared pointer reference to the parent element.
		*/
		std::shared_ptr<Element> parent;

		/**
		 * @brief Self Pointer
		 * 
		 * Shared pointer reference to the element itself.
		*/
		std::shared_ptr<Element> self;

		/**
		 * @brief Local UUID
		 * 
		 * Locally unique identifier of the object, which must start with OBJ-, FUN-, PAR-, or VAR- depending on the type.
		*/
		std::string uuid;

		/**
		 * @brief Name
		 * 
		 * Human-readable name of the element in string format.
		*/
		std::string name;

		/**
		 * @brief Description
		 * 
		 * Human-readable description of the element in string format. 
		*/
		std::string description;

		/**
		 * @brief Ontology identifier
		 * 
		 * Ontology identifier, can be set to null if no ontology is followed
		 * 
		 * @pre Referencing to an ontology prerequisites that an ontology in SOIL format has been declared elsewhere.
		*/
		std::string ontology;

		/**
		 * @brief Element Mutex
		 * 
		 * Recursive Mutex to provide thread safe access to SOIL elements.
		*/
		std::recursive_mutex mutex;

		/**
		 * @brief Constructor
		 * 
		 * Default constructor for SOIL elements, to be called from derived classes.
		 * @param [in] parent Shared pointer to parent object. Can be set to NULL for the creation of a root object
		 * @param [in] uuid Locally Unique identifier
		 * @param [in] name Human-readable name
		 * @param [in] description Human-readable description
		 * @param [in] ontology Ontology reference, is set to null if an empty string is passed
		*/

		Element(std::shared_ptr<Element> parent, std::string uuid, std::string name, std::string description, std::string ontology ="");
		
		/**
		 * @brief Destructor
		 * 
		 * Destructor for Element, which is declared virtual such that Element pointers can be used to delete instances of derived types.
		 * When an Element is deleted in SOIL, all references to the child objects are cleared.
		 * @post If the parent element was the last instance to contain shared pointers to the child elements, their lifecycle will end with this operation.
		*/
		virtual ~Element();

		/**
		 * @brief FQID
		 * 
		 * Constructs the FQID of the element by concatenating the UUIDs of the parent elements
		 * @return FQID string separated by /
		*/
		std::vector<std::string> fqid(void);

		/**
		 * @brief Access Operator
		 * 
		 * Returns the child matching the relative FQID of the element.
		 * If the empty string is passed, the element itself is resturned.
		 * 
		 * @exception std::runtime_error If no matching child is found, a runtime_error is thrown.
		 *
		 * @param [in] fqid Relative FQID of the element to retrieve
		 * @return Share pointer to the requested element
		*/
		std::shared_ptr<Element> operator[] (std::string fqid);

		/**
		 * @brief Add Child Element
		 * 
		 * Inserts a child to this element. Children are always managed as shared pointers, i.e. no object is copied.
		 * @param [in] uuid UUID to assign to the child, which also acts as internal identifier 
		 * @param [in] child Shared pointer to the element
		 * @return Shared pointer to child element
		*/
		std::shared_ptr<Element>  add(std::string uuid, std::shared_ptr<Element> child);
		
		/**
		 * @brief Add Child Element
		 *
		 * Inserts a child to this element. Children are always managed as shared pointers, i.e. no object is copied.
		 * This version takes a raw pointer (as e.g. returned by @c new) and internally makes a shared pointer.
		 * @param [in] uuid UUID to assign to the child, which also acts as internal identifier
		 * @param [in] child Pointer to the element
		 * @return Shared pointer to child element
		*/
		std::shared_ptr<Element>  add(std::string uuid, Element* child);

		/**
		 * @brief Add Child Element
		 *
		 * Alias to add provide compatibilty to prior versions.
		 * @todo Remove in future versions.
		 * @param [in] uuid UUID to assign to the child, which also acts as internal identifier
		 * @param [in] child Shared pointer to the element
		 * @return True if the element previously already existed.
		*/
		bool  insert(std::string uuid, std::shared_ptr<Element> child);

		/**
		 * @brief Add Child Element
		 *
		 * Alias to add provide compatibilty to prior versions.
		 * @todo Remove in future versions.
		 * This version takes a raw pointer (as e.g. returned by @c new) and internally makes a shared pointer.
		 * @param [in] uuid UUID to assign to the child, which also acts as internal identifier
		 * @param [in] child Pointer to the element
		 * @return True if the element previously already existed.
		*/
		bool  insert(std::string uuid, Element* child);

		/**
		 * @brief Remove Child element
		 * 
		 * Removes an element from the children of this element.
		 * If the internal shared pointer is the last reference to this element, it will go out of scope.
		 * @param [in] uuid UUID to identify the element to remove
		 * @return True if the element existed prior to deletion.
		*/
		bool remove(std::string uuid);

		/**
		 * @brief Get dynamically casted pointer
		 * 
		 * Dynamically casts the stored element reference to a given type.
		 * This useful for casting pointers to derived classes of which the type is known.
		 * 
		 * @tparam T Dervied type to cast to.
		 * @return Raw pointer
		*/
		template<typename T> T* cast(void);

		/**
		 * @brief Is Object?
		 * 
		 * Determines whether the element actually is an object.
		 * Internally, the prefix OBJ- of the UUID is checked for this purpose.
		 * This function is typically used before dynamic casts.
		 * @return Boolean response whether the element is an object.
		*/
		bool is_object(void) const;

		/**
		 * @brief Is Variable?
		 *
		 * Determines whether the element actually is a variable.
		 * Internally, the prefix VAR- of the UUID is checked for this purpose.
		 * This function is typically used before dynamic casts.
		 * @return Boolean response whether the element is at.
		*/
		bool is_variable(void) const;

		/**
		 * @brief Is Function?
		 *
		 * Determines whether the element actually is a function.
		 * Internally, the prefix FUN- of the UUID is checked for this purpose.
		 * This function is typically used before dynamic casts.
		 * @return Boolean response whether the element is a function.
		*/
		bool is_function(void) const;

		/**
		 * @brief Is Parameter?
		 *
		 * Determines whether the element actually is a parameter.
		 * Internally, the prefix PAR- of the UUID is checked for this purpose.
		 * This function is typically used before dynamic casts.
		 * @return Boolean response whether the element is a parameter.
		*/
		bool is_parameter(void) const;

		/**
		 * @brief HTTP JSON
		 * 
		 * Get a HTTP JSON object corresponding to the current state of the element.
		 * This function is virtual as normally it should be specialized by Object, Variable, Parameter and Function.
		 * @return JSON object
		*/
		virtual HTTP::Json wjson(void);

		/**
		 * @brief JSON string
		 *
		 * Get a JSON string corresponding to the current state of the element.
		 * This function internally useses wjson() such that there is no need to override both methods in derived classes.
		 * @return JSON string
		*/
		virtual std::string json(void);

		/**
		 * @brief HTTP Handler
		 * 
		 * Handles an incoming HTTP request as resource. It determines the correct child item (or the itdem itself) and than calls the handler
		 * of the HTTP::Resource base clase. The arguments are directly passed on to the latter.
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		HTTP::Response handle(HTTP::Request request, std::smatch match = std::smatch());

	};
	template<typename T>
	T* Element::cast(void)
	{
		return dynamic_cast<T*>(self.get());
	}

	/**
	 * @brief Null deleter
	 * 
	 * A function that essentially does nothing which is required as argument in the constructor of Element
	 * to avoid that delete is implicitly called a second time in the destructor when the member shared pointer to the
	 * object itself is deleted and the internal shared pointer is the last reference to the object. This situation occurs for
	 * root objects.
	 * @param  [in] ptr Pointer on which to do nothing
	*/
	inline void null_deleter(SOIL::Element* ptr) {};

}

