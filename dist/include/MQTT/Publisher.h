#pragma once
#include "constants.h"
#include "Configuration.h"
#include "MessageContainer.h"
#include <deque>
#include <atomic>
#include <mutex>
#include <thread>
#include <vector>
#include <condition_variable>
//#include <mqtt/async_client.h>


namespace mqtt
{
	/**
	 * @brief Forward Declare
	*/
	class async_client;
}

namespace MQTT
{
	/**
	 * @brief MQTT Publisher
	 * 
	 * This class abstracts the process of publishing MQTT messages to an MQTT broker.
	 * Currently, the underlying implementation is realized using Paho-MQTT. 
	 * Upon connection, it spans an own worker thread that continously publishes messages put into a message queue
	 * such that the calling thread does not get blocked with the communication process.
	 * The purpose of this class is pure publishing, so there is no subscribing functionality implemented. 
	 * 
	 * @todo Implement support for authenticating with client certificates.
	*/
	class DLL Publisher
	{
	private:
		/**
		 * @brief Number of class instances
		 * 
		 * Counter to track the number of class instances, which may be needed to initialize and clean
		 * the underlying library (was the case for mosquitto).
		*/
		static int instances;

		/**
		 * @brief Unique identifier
		 * 
		 * Unique identifier that is presented to the broker. Be careful if you reuse code across
		 * applications that some broker show an unexpected behaviour when using the same id more than once.
		*/
		std::string id;

		/**
		 * @brief Internal Message queue length
		 * 
		 * Maximum number of messages that may enter the internal message queue. 
		 * After that, no message with QoS=0 is accepted anymore to avoid the buildup
		 * of an eteneral delay. Limiting the size of the message queue avoids problems when
		 * data is produced faster as it can be published via MQTT (and meaningfully be consumed).
		*/
		unsigned int buffer;

		/**
		 * @brief Current configuration
		 * 
		 * Current configuration of the MQTT publisher. For details, please refer to the documentation of the respective class.
		*/
		Configuration configuration;

		/**
		 * @brief Internal client pointer
		 * 
		 * Shared pointer to the internal client of the underlying library.
		*/

		std::shared_ptr<mqtt::async_client> client;

		/**
		 * @brief Message Queue
		 * 
		 * Implemented message queue based on std::deque, which has no re-allocation costs upon extension.
		*/
		std::deque<MessageContainer> queue;

		/**
		 * @brief Brief Atomic Queue Size
		 * 
		 * Thread-safe, atomic variable to coordinate the queue.
		*/
		std::atomic<int> queue_size;

		/**
		 * @brief Message Queue Mutex
		 * 
		 * Mutex to protect the message queue between threads that are inserting messages and the processing loop.
		*/
		std::mutex queue_mutex;

		/**
		 * @brief Message Queue Condition Variable
		 * 
		 * Condition variable to synchronise the blockage of threads while simulatenously accessing the message queue.
		*/
		std::condition_variable queue_condition;

		/**
		 * @brief Worker Thread Pointer
		 * 
		 * Internal pointr to the worker thread running the publishing loop.
		*/

		std::shared_ptr<std::thread> worker;

		/**
		 * @brief  Worker thread status
		 * 
		 * Atomic boolean flag to indicate whether the worker thread is running.
		 * This variable is used to coordinate the starting and stopping process of the worker thread.
		*/
		std::atomic<bool> worker_running;

		/**
		 * @brief Worker loop
		 * 
		 * Internal loop function that is executed in the worker thread.
		 * The loop checks for new messages in the queue as often as possible and publishes them.
		*/

		void loop(void);

		/**
		 * @brief Resart worker thread
		 * 
		 * Internal function to (re)start the worker thread.
		 * This function can also be used to initially start the worker thread.
		*/
		void restart_worker(void);

		/**
		 * @brief Resart worker thread
		 *
		 * Internal function to stop the worker thread.
		*/
		void stop_worker(void);
		

	public:
		/**
		 * @brief Constructor 
		 *
		 * Constructor for MQTT publisher that should be called from user's code
		 * @param [in] id Unique identifier that is presented to the broker. Be careful if you reuse code across
		 * applications that some broker show an unexpected behaviour when using the same id more than once. 
		 * @param [in] buffer Size of the message queue before messages with QoS=0 are discarded.
		*/
		Publisher(std::string id, unsigned int buffer = 1024);
		
		/**
		* @brief Destructor
		* 
		* Default destructor
		*/
		~Publisher();

		/**
		 * @brief Publish an MQTT message
		 * 
		 * This is the core function to publish an MQTT message. It deposits a message into the queue.
		 * @param [in] topic Topic to which the message shall be sent.
		 * @param [in] message Primary content of the messsage to send.
		 * @param [in] qos MQTT Quality of service level to choose for the message.
		 * Check the MQTT specifications for the exact behaviours of 0, 1, and 2 in conjuncation with message retention.
		 * @param [in] retain Flag whether to retain the message on the broker after disconnection.
		 * @return True or false depending whether the message was accepted in the message queue (cf. buffer size).
		*/
		bool publish(std::string topic, std::string message, int qos, bool retain);

		/**
		 * @brief Publish multiple MQTT messages
		 *
		 * This is the multi-message version of the publish function, which deposits all messages at once into the message queue and gains performance by reducing the number of lock operations.
		 * @param [in] topic List of topics to which the messages shall be sent.
		 * @param [in] message List of messages, in the correpsonding order of the list of topics.
		 * @param [in] qos MQTT Quality of service level to choose for the message.
		 * Check the MQTT specifications for the exact behaviours of 0, 1, and 2 in conjuncation with message retention.
		 * @param [in] retain Flag whether to retain the message on the broker after disconnection.
		 * @return True or false depending whether the message was accepted in the message queue (cf. buffer size).
		*/
		bool publish(std::vector<std::string> topics, std::vector<std::string> messages, int qos, bool retain);

		/**
		 * @brief Connect to broker
		 * 
		 * Connect to the broker using the provided configuration object.
		 * The internal configuration object will be overridden. 
		 * 
		 * @param [in] configuration Configuration to apply.
		*/
		void connect(MQTT::Configuration configuration);

		/**
		 * @brief Connect to broker
		 * 
		 * Connect to the broker using the configuration object internally stored.
		*/
		void connect();

		/**
		 * @brief Direct Reconnect
		 * 
		 * Direct call of the internal reconnect function. No reconfiguration takes place,
		*/
		void reconnect(void);
		
		/**
		 * @brief Diconnect from the broker
		 * 
		 * Disconnect from the broker and internally stop the worker thread.
		 * This is a blocking call.
		 * 
		 * @param[in] timeout Timeout for the blocking operation in milliseconds.
		*/
		void disconnect(unsigned int timeout = 10000);

		/**
		 * @brief Set new root topic
		 * 
		 * Override the root topic. This can be done while connected as it only affects preprocessing of messages.
		 * @param [in] root_topic New root topic.
		*/
		void set_root_topic(std::string root_topic);

		/**
		 * @brief Set message queue size
		 * 
		 * Set a new size for the internal message queue.
		 * 
		 * @param[in] buffer New size
		*/
		void set_buffer(unsigned int buffer);

		/**
		 * @brief Is connected?
		 * 
		 * Checks whether the publisher is currently connected to a broker.
		 * Directly calls the underlying implementation of the library if possible.
		 * 
		 * @return True if connected, else false.
		*/
		bool is_connected(void);

		/**
		 * @brief Minimum delay between to messages in milliseconds
		 *
		 * A minimum delay which should be kept between sending two messages, provided in milliseconds.
		 * Defaults to @c 0 .
		 * @post This is not enforced in the client, but should be called as property of the publishing by the using code.
		 * @return Current delay set.
		*/
		inline int min_delay_ms(void) { return configuration.min_delay_ms; }

		/**
		 * @brief Register new configuration
		 * 
		 * Register a new configuration, without connecting.
		 * Changes will only take place after (re-)connecting to the broker.
		 * @param[in] configuration New configuration object.
		*/
		void configure(MQTT::Configuration configuration);


	};
}


