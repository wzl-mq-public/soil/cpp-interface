#pragma once
#include "constants.h"
#include "Types.h"
#define _TURN_OFF_PLATFORM_STRING
#include "cpprest/http_listener.h"
#include <regex>
#include <vector>

namespace HTTP {

	/**
	 * @brief HTTP Resource base class
	 * 
	 * Base class for any resource that is implemented using this library.
	 * It should always be subclassed by the class implmenting the resource itself,
	 * and the relevant functions should be overriden. All HTTP business logic internally
	 * relies on the @em cpprestsdk library. The former internally uses widestrings, 
	 * which leads to the need for conversion work at some places.
	*/

	class DLL Resource
	{
	private:
		/**
		 * @brief Fallback response method
		 * 
		 * Fallback method to answer requests for which no method has been implemented.
		 * It will return a response containing details about the request which may be helpful
		 * for debugging. The HTTP code is set to 501 (Not Implemented)
		 * 
		 * @param message[in] Incoming HTTP request as preprocessed by cpprestsdk
		 * @param match[in] Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		Response fallback(Request message, std::smatch match = std::smatch());
	protected:
		/**
		 * @brief Allowed methods
		 * 
		 * List of allowed HTTP methods for this resource implemented as @c std::string
		 * Defaults to all methods (GET, DELETE, PATCH, POST, OPTIONS, HEAD, PUT)
		*/
		std::vector<web::http::method> allowed_methods;

		/**
		 * @brief Content type
		 *
		 * Content type that is delivered by this resource. 
		 * Defaults to @c application/json
		*/
		std::string content_type;

		/**
		 * @brief Allowed Origins
		 * 
		 * Origins that are allowed, relavant when implementing web clients and running into CORS issues.
		 * Defaults to @c * .
		 * 
		 * @post If you want to be serious about security, you should limit the origins here.
		*/
		std::string allowed_origins;

		/**
		 * @brief Apply headers
		 * 
		 * Apply headers to the HTTP response object.
		 * This function shoud be called in the handler before returning the repsonse object to the server. 
		 * @param[in, out] response Response object to act on.
		*/
		virtual void apply_headers(Response& response);
	public:
		/**
		 * @brief Constructor
		 * 
		 * Default Constructor, sets the above mentioned defaults for allowed_methods, content_type and allowed_origins
		*/
		Resource();

		/**
		 * @brief Default Destructor
		 * 
		 * Default destructor, does no custom business logic.
		*/
		~Resource();

		/**
		 * @brief Request Info
		 * 
		 * Extracts all sort of information from the request and returns it in JSON format.
		 * This is useful for fallback methods.
		 * 
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * 
		 * @return Request information in JSON format
		*/
		static web::json::value request_info(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP Handler
		 * 
		 * Handler that is called by the server on HTTP requests.
		 * By default, this function redirects to the more specifc ones based on the respective HTTP Verb.
		 * In case an exception occurs, it calls the exception handler.
		 * 
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP GET Handler
		 *
		 * Handler that is called by the server on HTTP requests on a GET method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_get(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP PUT Handler
		 *
		 * Handler that is called by the server on HTTP requests on a PUT method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_put(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP POST Handler
		 *
		 * Handler that is called by the server on HTTP requests on a POST method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_post(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP DELETE Handler
		 *
		 * Handler that is called by the server on HTTP requests on a DELETE method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_delete(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP PATCH Handler
		 *
		 * Handler that is called by the server on HTTP requests on a PATCH method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_patch(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP OPTIONS Handler
		 *
		 * Handler that is called by the server on HTTP requests on an OPTIONS method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_options(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP HEAD Handler
		 *
		 * Handler that is called by the server on HTTP requests on a HEAD method.
		 * This function should be overridden by the implementation of the resource.
		 * Per default, it resorts to the internal fallback function.
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_head(Request message, std::smatch match = std::smatch());

		/**
		 * @brief HTTP Exception handler
		 *
		 * Handler function which can be called inside other handler functions to manage exceptions.
		 * It returns the request's information and exeption error text while setting the HTTP response to 500 (Internal Error).
		 *
		 * @param [in] message Incoming HTTP request as preprocessed by cpprestsdk
		 * @param [in] exception Exception to handle and to copy the error message from.
		 * @param [in] match Match result of the request path that led to this resource
		 * @return Outgoing HTTP response to be processed by cpprestdsk
		*/
		virtual Response handle_exception(Request message,  std::exception& exception, std::smatch match = std::smatch());
	};
}

