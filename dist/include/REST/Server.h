#pragma once
#include "constants.h"
#include "Resource.h"
#define _TURN_OFF_PLATFORM_STRING
#include "cpprest/http_listener.h"


using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;

namespace HTTP
{
	/**
	 * @brief HTPP Server
	 * 
	 * Abstraction layer for an HTTP Server implemented using 
	 * the cpprestsdk.
	 * 
	 * @todo cpprestsdk is now in maintenance mode, if this library shall be in the very long term, a substitute may need to be found.
	*/
	class DLL Server
	{
	private:
		/**
		 * @brief Default resource
		 * 
		 * Empty default resource which acts as a fallback to server requests.
		*/
		Resource default_resource;

		/**
		 * @brief Resource list
		 * 
		 * List of resources, which is implemented as paths and pointer to the according objects.
		 * The order of the items in the vector is important when considering matching precedence.
		*/
		std::vector<std::pair<std::string, std::shared_ptr<Resource> > > resources;

		/**
		 * @brief Main request handler
		 * 
		 * This is the main handler for incoming requests. It processes in the following order:
		 * - Run through the registered resources until the first regular expression match is encountered
		 * - Call the handler on the matched resources
		 * - If no resource is matched, call the default resoruce
		 * - Reply to the request with the obtained answer
		 * @param message 
		*/
		void handle(http_request message);


		/**
		 * @brief Internal listener
		 * 
		 * Internal listener object as provided by the cpprestsdk library.
		*/
		http_listener listener;

	public:
		/**
		 * @brief Constructor
		 * 
		 * Constructor setting up the server, but not accepting connections upon construction.
		 * @param [in] url Base URL, e.g. @c http://localhost:8000. Please note that the hostname part indicates the interface on which the server will listen.
		 * 
		 * @post When using another interface than localhost, Windows will prompt for firewall permissions. In general firewalls should be considered when working with HTTP Servers.
		 * @post HTTPS is currently not implemented here - The current recommendation is that a reverse proxy is used for this purpose, e.g. NGINX or Microsoft IIS.
		*/
		Server(std::string url);

		/**
		 * @brief Destructor
		 * 
		 * Standard Destructor without custom logic.
		 * The server will be closed upon destruction, so make sure that the lifecycle of the server objects is apropriately managed in your application.
		*/
		~Server();

		/**
		 * @brief Start listener
		 * 
		 * Open the server for incoming connections. This is a blocking call.
		*/
		inline void open() { listener.open().wait(); }

		/**
		 * @brief Stop listener
		 *
		 * Open the server for incoming connections. This is a blocking call.
		*/
		inline void close() { listener.close().wait(); }

		/**
		 * @brief Register resource
		 * 
		 * Register a resource by means of a matching expression and shared pointer reference.
		 * Please note that the order of adding resources to the sever determines the precedence in matching.
		 * The frst matching resource will handle the request. This is a common source of errors.
		 * This function can be called dynamically at runtime, also when the server is already running.
		 * 
		 * @param [in] path Path of the resource written as regular expression. Use @c L"/?(.*)" to match everything under the root path. The path simulatenously acts as unique identifier.
		 * @param [in] resource Shared pointer to the resource that shall handle this path. Please be aware of the lifecycle in case you interfere with the pointers.
		*/
		void add(std::string path, std::shared_ptr<Resource> resource);

		/**
		 * @brief Register resource
		 *
		 * Remove a previously registered resource.
		 * This function can be called dynamically at runtime, also when the server is already running.
		 *
		 * @param [in] path Path of the resource written as regular expression. Use @c L"/?(.*)" which was used to add the resource and acts an unique identifier.
		*/
		void remove(std::string path);

	};
}

