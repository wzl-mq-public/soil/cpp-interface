#pragma once
#define _TURN_OFF_PLATFORM_STRING
#include <cpprest/http_listener.h>

namespace HTTP
{
	/**
	 * @brief HTTP Request
	 * 
	 * Typedefinition for HTTP Requests as provided by the cpprestsdk library.
	*/
	typedef web::http::http_request Request;

	/**
	 * @brief HTTP Response
	 *
	 * Typedefinition for HTTP Responses as provided by the cpprestsdk library.
	*/
	typedef web::http::http_response Response;

	/**
	 * @brief HTTP Status
	 *
	 * Typedefinition for HTTP Status Codes as provided by the cpprestsdk library.
	*/
	typedef web::http::status_codes Status;

	/**
	 * @brief HTTP Methods
	 *
	 * Typedefinition for HTTP Methods (Enum) as provided by the cpprestsdk library.
	*/
	typedef web::http::methods Methods;

	/**
	 * @brief HTTP JSON
	 *
	 * Typedefinition for Json documents as provided by the cpprestsdk library.
	*/
	typedef web::json::value Json;

}
