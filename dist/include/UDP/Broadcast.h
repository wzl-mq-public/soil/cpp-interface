#pragma once

#include "constants.h"
#include <atomic>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <vector>
#include "Configuration.h"
#include <boost/thread.hpp>
#include <boost/asio.hpp>

namespace UDP
{
	/**
	 * @brief UDP Broadcast
	 * 
	 * Simple UDP Broadcast implemented on top of boost asio.
	 * This is currently a conveience member of the C++ SOIL library as its not part of the specification.
	 * It may be omitted in the future.
	*/
	class DLL Broadcast
	{
	private:
		/**
		 * @brief Thread group
		 * 
		 * Internal group of threads handling the communication work
		 * The boost asio library organized asynchronous communication by itself so that blocking calls can be avoided.
		*/
		std::shared_ptr<boost::thread_group> threads;

		/**
		 * @brief ASIO work object
		 * 
		 * Work object required for boost asio to maintain threads alive
		*/
		std::shared_ptr<boost::asio::io_service::work> work;

		/**
		 * @brief ASIO IO service
		 * 
		 * Boost ASIO IO service obejct used to implement UDP communication
		*/
		boost::asio::io_service io_service;

		/**
		 * @brief UDP Socket pointer
		 * 
		 * Shared pointer to the internally used udp socket implemented using boost.
		*/
		std::shared_ptr<boost::asio::ip::udp::socket> socket;

		/**
		 * @brief UDP Endpoints
		 * 
		 * List of UDP endpoints which are recipients of the broadcast
		*/
		std::vector<boost::asio::ip::udp::endpoint> endpoints;

		/**
		 * @brief UDP Endpoints mutex
		 * 
		 * Mutex to make access to the list of endpoints thread safe when adding or removing clients.
		*/
		std::mutex endpoints_mutex;

		/**
		 * @brief Internal Send
		 * 
		 * Send function called internally and implementing the boost asio specific logic.
		 * @param [in] message Message to broadcast over UDP
		*/
		void _send(std::string message);

	public:
		/**
		 * @brief Constructor
		 * 
		 * Constructor instantiating an UDP Broadcast.
		 * The socket is immediately opened upon successful construction.
		 * 
		 * @param[in] n_threads Number of threads to assign to the thread group.
		 * Only for very large loads a number of threads greater than 1 should be needed.
		*/
		Broadcast(int n_threads = 1);

		/**
		 * @brief Destructor
		 * 
		 * Destructor which closes the socket and stops all threads if not previously done.
		*/
		~Broadcast();

		/**
		 * @brief Add UDP client
		 * 
		 * Add a client to list of recipients of the UDP broadcast.
		 * One can only send to one port per IP address, the IPv4 address acts as unique identifier.
		 * 
		 * @param [in] ip IPv4 address to send to
		 * @param [in] port Port to send to 
		*/
		void add_client(std::string ip, unsigned int port);
		
		/**
		 * @brief Remove UDP client
		 * 
		 * Remove a client to list of recipients of the UDP broadcast which is identified through its IPv4 address.
		 * @param [in] ip IPv4 adress of the client to remove 
		*/
		void remove_client(std::string ip);

		/**
		 * @brief Send Message
		 * 
		 * Send an individual message over the UDP Broadcast.It will be terminated by a newline (@c \r\n).
		 * @param [in] message Message to send
		*/
		void send(std::string message);

		/**
		 * @brief Send Multiple Message
		 *
		 * Send a set of messages over the UDP Broadcast.They will be separated and terminated by a newlines (@c \r\n).
		 * @param [in] messages Messages to send
		*/
		void send(std::vector<std::string> messages);

		//void handle(std::shared_ptr<std::string> message, const boost::system::error_code& error, std::size_t bytes);
		
		/**
		 * @brief Number of Clients
		 * 
		 * Get the number of registered UDP clients.
		 *
		 * @return Number of registered UDP clients.
		*/
		inline int use_count(void) { return static_cast<int>(endpoints.size()); }

		/**
		 * @brief Configure Broadcast
		 * 
		 * Configure the broadcast with a configuration object, i.e. a list of endpoints.
		 * @param [in] config Configuration object 
		*/
		void configure(UDP::Configuration config);
	};
}

