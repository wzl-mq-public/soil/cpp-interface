#pragma once
#include "constants.h"
#include <stdexcept>
namespace UDP
{
	/**
	 * @brief UDP Broadcast Exception
	 *
	 * Specific subclass of @c std::runtime_error to distinguish exceptions coming from the UDO Broadcast.
	*/
	class DLL Exception :
		public std::runtime_error
	{
	public:
		/**
		 * @brief Constructor
		 * 
		 * Constructor taking a message that is passed to @c std::runtime_error.
		 * @param [in] message Message to add to the runtime error
		*/
		Exception(const char* message = "");

		/**
		 * @brief Destructor
		 * 
		 * Default destructor, no special effort here.
		*/
		~Exception(void);

	};
}