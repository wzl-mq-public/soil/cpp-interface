#pragma once
#include "constants.h"
#include <vector>
#include <string>

namespace SIGN
{
	/**
	 * @brief SHA256 Hasher
	 * 
	 * Class which provides an convenient interface for calculating SHA256 hashes.
	 * The underlying methods are taken from OpenSSL.
	*/
	class DLL Hasher
	{
	private:
		/**
		 * @brief Internal data
		 * 
		 * Internal data buffer based on unsigned char as byte type.
		*/
		std::vector<unsigned char> data;
	public:
		/**
		 * @brief Constructor
		 * 
		 * Default constructor, no special effort here.
		*/
		Hasher();

		/**
		 * @brief Destructor
		 * 
		 * Default destructor, no special effort here.
		*/
		~Hasher();

		/**
		 * @brief Add data
		 * 
		 * Push back data to the internal buffer which eventually gets hashed.
		 * This function is implemented using templates, underneath it copies the raw bytes after casting.
		 * 
		 * @tparam T Original type of the item to add to the buffer
		 * @param [in] x Item to add to the buffer
		*/
		template <typename T> void push_back(T x);
		
		/**
		 * @brief Hash the data buffer
		 * 
		 * Calculate the SHA256 hash of the current data buffer and return it as standard vector of bytes.
		 * Internally calls the static SHA256 function 
		 * @post The output of this function can be used in the signer.
		 * @return Hash result as sequenxe of bytes.
		*/
		std::vector<unsigned char> hash();

		/**
		 * @brief Reset data buffer
		 * 
		 * Clear the internal data buffer (i.e. to start with new data)
		*/
		void reset();

		/**
		 * @brief Digest size
		 * 
		 * Convenience function to get the digest size, which is often needed when handling raw bytes.
		 * @return SHA256 digest length
		*/
		size_t size(void);

		/**
		 * @brief Print Bytestring
		 * 
		 * Convenience function to pretty-print bytestrings in HEX format.
		 * 
		 * @param [in] bytes Data to print
		 * @param [in] uppercase Boolean flag whether to print uppercase
		 * @return String with print result.		*/
		
		static std::string print(std::vector<unsigned char> bytes, bool uppercase = true);

		/**
		 * @brief SHA256 hash
		 * 
		 * Calculate the SHA256 hash of the given input data.
		 * Be careful to no pass invalid pointers here, there is a risk of memory leaks.
		 * 
		 * @param data [in] Pointer to the memory block to read from 
		 * @param length [in] Length of the data to consume
		 * @return SHA256 hash as vector of bytes
		*/
		static std::vector<unsigned char> sha256(const unsigned char* data, size_t length);
	};

	template<typename T>
	inline void Hasher::push_back(T x)
	{
		unsigned char* pointer = reinterpret_cast<unsigned char*>(&x);
		for (int i = 0; i < sizeof(T); i++)
		{
			data.push_back(pointer[i]);
		}
	}
}
